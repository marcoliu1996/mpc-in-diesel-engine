function [Linv, F, J, c, W, A, B] = get_controller_from_opcon(Ne_rpm, w_fuel)
%This function obtains the required matrices for a local MPC controller
%about an operating point
%The Q, P, R matrices are by default set to heuristic values
%Prediction horizon by default set to 10

%Discrete-time linear model
[A, B] = get_9state_linmodel(Ne_rpm, w_fuel);

speed_rpm = Ne_rpm;
fuel_mm3str = w_fuel;

[u_ss, x_ss, converged] = get_ss_inputs(speed_rpm, fuel_mm3str);

%State Constraints
x_lb = [80; 0; 80; 0; 80; 0; 80; 0; 0];
x_ub = [1000; 5; 1000; 5; 1000; 5; 1000; 20; 200];

xmin = x_lb - x_ss;
xmax = x_ub - x_ss;

%Input constraints
u_lb = [0; 0; 20];
u_ub = [95; 90; 95];

umin = u_lb - u_ss;
umax = u_ub - u_ss;

%Weigting to account for units
weighting = diag([0.01 1 0.01 1 0.01 1 0.01 0.2 0.1]);

n = length(A); %number of states
m = size(B, 2); %number of inputs

%Cost weight matrices
Q = weighting*1*eye(n);
R = 0.1*eye(m);
P = dare(A,B,Q,R);

N = 10; %prediction horizon

%%

%Populate Phi matrix by concatenation
Phi = [];
for i = 1:N
    Phi = [Phi; A^i];
end

%Populate Gamma matrix by concatenating down the rows, then concatenating
%the columns
Gamma  = [];
for j = 1:N
    gamma = [];
    for i = 1:N
        if j > i
            gamma = [gamma; zeros(size(B))];
        else
            gamma = [gamma; A^(i - j)*B];
        end
    end
    Gamma = [Gamma, gamma];
end

%Populate constraint vector by concatenation
c = [];
for i = 1:N
    fi = [xmax; -xmin; umax; -umin];
    c = [c; fi];
end
h = [xmax; -xmin]; %Terminal constraints
c = [c; h];

%Populate constaint matrix on states by block diagonal concatenation
M = [];
for i = 1:(N - 1)
    Mi = [eye(n); -eye(n); zeros(m, n); zeros(m, n)];
    M = blkdiag(M, Mi);
end
Gt = [eye(n); -eye(n)]; %Terminal constraint matrix on states
M = blkdiag(M, Gt);
%Append zeros above to match dimension of the constraint vector
M = [zeros(size(c, 1) - size(M, 1), size(M, 2)); M];

%Populate constaint matrix on inputs by block diagonal concatenation
E = [];
for i = 1:N
    Ei = [zeros(n, m); zeros(n, m); eye(m); -eye(m)];
    E = blkdiag(E, Ei);
end
%Append zeros below to match dimension of the constraint vector
E = [E; zeros(size(c, 1) - size(E, 1), size(E, 2))];

%Constraint matrix on initial state
D = [eye(n); -eye(n); zeros(m, n); zeros(m, n)];
%Append zeros below to match dimension of the constraint vector
D = [D; zeros(size(c, 1) - size(D, 1), size(D, 2))];

%Compute final inequality constraint matrices
J = M*Gamma + E;
W = -M*Phi - D;

%%

%Populate Psi matrix by block diagonal concatenation
Psi = [];
for i = 1:N
    Psi = blkdiag(Psi, R);
end

%Populate Omega matrix by block diagonal concatenation
Omega = [];
for i = 1:(N - 1)
    Omega = blkdiag(Omega, Q);
end
Omega = blkdiag(Omega, P); %Terminal state weight

%Compute the Hessian
G = 2*(Gamma'*Omega*Gamma + Psi);
G = (G + G')/2; %Force symmetric

%Compute the other term in the cost function
F = 2*Gamma'*Omega*Phi;

[L,~] = chol(G, 'lower');
Linv = inv(L);
if ~istril(Linv) %Force lower triangular to pass integrity check
    Linv = tril(Linv);
end