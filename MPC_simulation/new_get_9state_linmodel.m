function [Ad, Bd] = new_get_9state_linmodel(Ne_rpm, w_fuel,u_ss,x_ss)
%This function returns the discrete-time A and B matrices for a 9 state
%linearised model about an operating condition, specified by a given engine
%speed (rpm) and fuel rate (mm^3/str)


%% Get steady state input and states


% u_ss = get_ss_inputs(Ne_rpm, w_fuel);
% 
% [x_ss, y_ss, x4_ss, converged] = sim_steady_state(u_ss, Ne_rpm, w_fuel);

%% Linearisation

smp8ms   = 0.008192;    %    8ms
smp16ms  = 0.016384;    %   16ms

fcn_x = @(x) fcn_MVEM(x, u_ss, [Ne_rpm, w_fuel]);
fcn_u = @(u) fcn_MVEM(x_ss, u, [Ne_rpm, w_fuel]);

A = jacobianest(fcn_x, x_ss);
B = jacobianest(fcn_u, u_ss);
C = eye(9);
D = zeros(9,3);

sys = ss(A,B,C,D);
sysd = c2d(sys,smp16ms,'zoh');

Ad = sysd.A;
Bd = sysd.B;
