clc
clear
close all
load('EUDC.mat')

linearPoint = 12;

%Process operating point trajectory by smoothing and saturating

speed_ref = min(max(smoothdata(speed, 'movmean', 26), 800), 2400);
fuel_ref = min(max(smoothdata(fuel, 'movmean', 26), 5), 80);

% speed_ref = speed;
% fuel_ref = fuel;
time = time - time(1);

%%==subsample to 16ms
subsample = 1:2:length(time);

speed_ref = speed_ref(subsample);
fuel_ref = fuel_ref(subsample);
time = time(subsample);

%get reference trajectory of states and outputs using steady state maps
% [x_ref, y_ref, u_ref, x4_ref] = steady_state_maps(speed_ref, fuel_ref);
[x_ref, y_ref, u_ref, x4_ref] = new_of_steady_state_maps(speed_ref, fuel_ref);

%% Open Loop Simulation

x_ol = zeros(9, length(time));
y_ol = zeros(2, length(time));

x_ol(:, 1) = x_ref(:, 1);
y_ol(:, 1) = y_ref(:, 1);
smp16ms  = 0.016384;    %   16ms

tic
for k = 1:(length(time) - 1)
    x_ol(:, k + 1) = x_ol(:, k) + smp16ms*fcn_MVEM(x_ol(:, k), u_ref(:, k), [speed_ref(k); fuel_ref(k)]);
    [~, y_ol(:, k + 1)] = fcn_MVEM(x_ol(:, k + 1), u_ref(:, k + 1), [speed_ref(k + 1); fuel_ref(k + 1)]);
end
toc