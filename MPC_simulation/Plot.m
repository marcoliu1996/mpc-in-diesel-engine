% for i = 1:9
% figure(i)
% % plot(time, y_ref(2, :))
% hold on
% plot(time, x_ref(i, :),'k')
% plot(time, x_ol(i, :),'r')
% 
% xlabel('Time (s)')
% ylabel(['x_state',num2str(i)])
% xlim([0 380])
% legend({ 'ref', 'ol', 'mpc'})
% 
% end

for i = 1:9
figure(i)
% plot(time, y_ref(2, :))
hold on
plot(time, x_ref(i, :),'k')
plot(time, x_ol(i, :),'r')
plot(time, x_mpc(i, :),'b')
plot(time, x_mpc_explicit(i, :),'g')
xlabel('Time (s)')
ylabel(['x_state',num2str(i)])
xlim([0 380])
legend({ 'ref', 'ol', 'mpc'})

end


% for i = 1:2
% figure(i+9)
% % plot(time, y_ref(2, :))
% hold on
% plot(time, y_ref(i, :))
% plot(time, y_mpc(i, :))
% xlabel('Time (s)')
% ylabel(['y',num2str(i)])
% xlim([0 380])
% legend({ 'ref', 'MPC'})
% 
% end

% for i = 1:3
% figure(i+11)
% % plot(time, y_ref(2, :))
% hold on
% plot(time, u_mpc(i, :))
% xlabel('Time (s)')
% ylabel(['u',num2str(i)])
% xlim([0 380])
% legend({ 'ref', 'MPC'})
% 
% end