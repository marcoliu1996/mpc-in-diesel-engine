clc
clear
close all

%This script demonstates closed loop reference tracking control over
%drivecycles using MPC. Results are not that great at the moment, and can
%still take a few minutes to fully simulate.

%load drivecycle

load('EUDC_Process.mat')
smp16ms  = 0.016384;    %   16ms


%% Generate Controllers at linearisation points

%Grid of linearisation points
speedlinpt = [800, 1600, 2400];
fuellinpt = [5, 30, 55, 80];
[speedlingrid, fuellingrid] = meshgrid(speedlinpt, fuellinpt);
Cd =eye(9);  
n = 1;
p= 10;
m=2;
Q =30*diag([0.1 0.1 0.0001 0.0001 0.0000001 0.00001 0.1 0.0001 0.1]);       
% Q =50*diag([0.1 0.0001 0.0001 0.0001 0.0001 0.0001 0.0001 0.0001 0.1]);     
R= 1*ones(1,3);
% Q =diag([0.01 1 0.01 1 0.01 1 0.01 0.2 0.1]);
LinearPoints = [800 5;800 30;800 55;800 80;1600 5;1400 22;1600 55;1600 80;2400 5;2400 30;2400 55;2400 80;1300 13;1105 22;1100 25;1282 27;2120 29;1990 5;1318 14;1500 38;1400 46;1500 45;1585 47;1800 39;1412 21;1412 22;1412 23;1312 5;1480 5;1700 49;1431 50.5;1461 47;1763 52;1936 55;2011 39;2008 49;2002 55;2027 62;2183 66;2400 63;2400 71;2400 76;2356 76;2268 73;1167 5];
%LinearPoints = [800 5;800 30;800 55;800 80;1600 5;1400 22;1600 55;1600 80;2400 5;2400 30;2400 55;2400 80;1300 13;1105 22;1100 25;1282 27;2120 29;1990 5;1318 14;1500 38;1400 46;1500 45;1585 47;1800 39;1412 21;1412 22;1412 23];
%LinearPoints = [800 5;800 30;800 55;800 80;1600 5;1600 55;1600 80;2400 5;2400 30;2400 55;2400 80;1105 22;1100 25;1282 27;2120 29;1990 5;1500 38;1400 46;1500 45;1585 47;1800 39;1700 49;1431 50.5;1461 47;1763 52;1936 55;2011 39;2008 49;2002 55;2027 62;2183 66;2400 63;2400 71;2400 76;2356 76;2268 73;2010 36];

for n = 1:length(LinearPoints)
        speed = LinearPoints(n,1);
        fuel = LinearPoints(n,2);
        [x_ref11, y_ref11, u_ref11, x4_ref11] = steady_state_maps(speed,fuel);
        sys = new_get_9state_linmodel2(speed, fuel,u_ref11,x_ref11);
        mpcobj = mpc(sys,smp16ms,p,m);
        mpcobj.Weights.OutputVariables = Q;
        mpcobj.Weights.ManipulatedVariablesRate = R;
        
        refsequence = [];
        for  j = 1:p
            refsequence  = [refsequence x_ref11];
        end
        controllers(n) = struct('matrixA', sys.A, 'matrixB', sys.B, 'OperatingPoint',[speed/30 fuel],'MPC',mpcobj,'RefSequence',refsequence,'feedforwardU',u_ref11);
        n = n+1;
end

controllerNum = n-1;
% 

%% Closed Loop MPC simulation

x_mpc = zeros(9, length(time));
x_mpc_explicit = zeros(9, length(time));
x_mpc_ss = zeros(9, length(time));

y_mpc = zeros(2, length(time));
y_mpc_explicit= zeros(2, length(time));
y_mpc_ss= zeros(2, length(time));


u_mpc = zeros(3, length(time));
u_mpc_explicit = zeros(3, length(time));
u_mpc_ss = zeros(3, length(time));
 
x_ref = x_ol;
x_mpc(:, 1) = x_ref(:, 1);
y_mpc(:, 1) = y_ref(:, 1);
x_mpc(:, 2) = x_ref(:, 2);
y_mpc(:, 2) = y_ref(:, 2);
y_mpc_explicit(:, 6500) = y_ref(:, 6500);
y_mpc_explicit(:, 6501) = y_ref(:, 6501);
x_mpc_explicit(:, 6500) = x_ref(:, 6500);
x_mpc_explicit(:, 6501) = x_ref(:, 6501);
y_mpc_ss(:, 1) = y_ref(:, 1);
y_mpc_ss(:, 2) = y_ref(:, 2);
x_mpc_ss(:, 1) = x_ref(:, 1);
x_mpc_ss(:, 2) = x_ref(:, 2);
u_mpc(:, 1) = u_ref(:, 1);
u_mpc_explicit(:, 6500) = u_ref(:, 6500);
u_mpc_ss(:, 1) = u_ref(:, 1);

u=[0;0;0];
u_explicit= [0;0;0];
control = [];
     
%Options for MPC QP solver
% options = mpcqpsolverOptions;
% options.IntegrityChecks = false; %makes it faster but risks runtime errors

tic
for k = 6501:(length(time) - 1-p)
     k
% % %     @x- u-
%     x_ref_sequence = [];
%     for  j = 1:p
%         x_ref_sequence = [x_ref_sequence x_mpc(:,k)];
%     end
%     x_ref_nominal = x_ref(:, k+1:k+p)-x_ref_sequence;  
%     sys = new_get_9state_linmodel2(speed_ref(k), fuel_ref(k),u_mpc(:,k-1),x_mpc(:,k));
%     mpcobj = mpc(sys,smp16ms,p,m);
%     mpcobj.Weights.ManipulatedVariablesRate = R;
%     mpcobj.Weights.OutputVariables =Q;
%     x = mpcstate(mpcobj);
%     [u,info] = mpcmove(mpcobj, x,zeros(9,1),x_ref_nominal',zeros(9,1)); 
%     u_mpc(:, k ) = u + u_mpc(:, k-1 );
%     x_mpc(:, k + 1) = x_mpc(:, k) + smp16ms*fcn_MVEM(x_mpc(:, k), u_mpc(:,k), [speed_ref(k); fuel_ref(k)]);
%     [~, y_mpc(:, k + 1)] = fcn_MVEM(x_mpc(:, k + 1), u_mpc(:,k), [speed_ref(k + 1); fuel_ref(k + 1)]);
%     
%     
%     
%     
%     
% %     @x_ref u_ref
%     x_ref_sequence_ss = [];
%     for  j = 1:p
%         x_ref_sequence_ss = [x_ref_sequence_ss x_ref(:,k)];
%     end 
%     x_ref_nominal_ss = x_ref(:, k+1:k+p)-x_ref_sequence_ss ; 
%     sys_ss = new_get_9state_linmodel2(speed_ref(k), fuel_ref(k),u_ref(:,k),x_ref(:,k));
%     mpcobj_ss = mpc(sys_ss,smp16ms,p,m);
%     mpcobj_ss.Weights.ManipulatedVariables = R;
%     mpcobj_ss.Weights.OutputVariables =Q;
%     xmpc_ss = mpcstate(mpcobj);
%     [u_deltass,info] = mpcmove(mpcobj_ss, xmpc_ss,x_mpc_ss(:, k )-x_ref(:,k),x_ref_nominal_ss',zeros(9,1)); 
%     u_mpc_ss(:, k ) = u_deltass + u_ref(:, k );
%     x_mpc_ss(:, k + 1) = x_mpc_ss(:, k) + smp16ms*fcn_MVEM(x_mpc_ss(:, k), u_mpc_ss(:,k), [speed_ref(k); fuel_ref(k)]);
%     [~, y_mpc_ss(:, k + 1)] = fcn_MVEM(x_mpc_ss(:, k + 1), u_mpc_ss(:,k), [speed_ref(k + 1); fuel_ref(k + 1)]);

    
    
    
    
    
%     @x_ref u_ref explicit
%     find the nearest linearization point
    operatingpoint = [speed_ref(k)/30 fuel_ref(k)];
    min = 99999;
    n = 0;
    for i = 1:controllerNum
        if norm(operatingpoint-controllers(i).OperatingPoint)<min
            min = norm(operatingpoint-controllers(i).OperatingPoint);
            n = i;
            
        end
    end
    
    %after found which controller, initialize variables of that controller
     control = [control n];
     mpcobj = controllers(n).MPC;
     x_ref_sequence_explicit = controllers(n).RefSequence;
     
    x_ref_nominal_explicit = x_ref(:, k+1:k+p)-x_ref_sequence_explicit;
    
    explicit_mpc = controllers(n).MPC;
    x_explicit = mpcstate(explicit_mpc);
    [u_explicit,info] = mpcmove(explicit_mpc, x_explicit,x_mpc_explicit(:, k )-x_ref_sequence_explicit(:,1),x_ref_nominal_explicit',zeros(9,1));
    u_mpc_explicit(:, k )=u_explicit+controllers(n).feedforwardU;
    x_mpc_explicit(:, k + 1) =x_mpc_explicit(:, k) + smp16ms*fcn_MVEM(x_mpc_explicit(:, k),u_mpc_explicit(:,k), [speed_ref(k); fuel_ref(k)]);
    [~, y_mpc_explicit(:, k + 1)] = fcn_MVEM(x_mpc_explicit(:, k + 1), u_mpc_explicit(:,k), [speed_ref(k + 1); fuel_ref(k + 1)]);


end
toc


for i = 1:9
figure(i)
% plot(time, y_ref(2, :))
hold on
plot(time, x_ref(i, :),'k')
%lot(time, x_ol(i, :),'r')
%plot(time, x_mpc(i, :),'b')
plot(time, x_mpc_explicit(i, :),'g')
%plot(time(1:k-1), control(1:k-1)*2,'m')
xlabel('Time (s)')
ylabel(['x_state',num2str(i)])
xlim([0 380])
legend({ 'ref', 'ol', 'mpc'})

end


