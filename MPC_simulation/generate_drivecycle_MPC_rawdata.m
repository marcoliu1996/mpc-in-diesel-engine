clc
clear
close all

%This script demonstates closed loop reference tracking control over
%drivecycles using MPC. Results are not that great at the moment, and can
%still take a few minutes to fully simulate.

%load drivecycle
load('EUDC.mat')

%Process operating point trajectory by smoothing and saturating

speed_ref = smoothdata(speed, 'movmean', 26);
fuel_ref = smoothdata(fuel, 'movmean', 26);

% speed_ref = speed;
% fuel_ref = fuel;
time = time - time(1);

%subsample to 16ms
subsample = 1:2:length(time);

speed_ref = speed_ref(subsample);
fuel_ref = fuel_ref(subsample);
time = time(subsample);
x_ref(:,1) = steady_state_maps(speed_ref(1), fuel_ref(1));
%get reference trajectory of states and outputs using steady state maps
for i = 2: length(time);
[x_ref(:,i), y_ref(:,i), u_ref(:,i), x4_ref(:,i)] = new_of_steady_state_maps(speed_ref(i), fuel_ref(i),x_ref(:,i-1));
end
"haha"
%% Open Loop Simulation

x_ol = zeros(9, length(time));
y_ol = zeros(2, length(time));

x_ol(:, 1) = x_ref(:, 1);
y_ol(:, 1) = y_ref(:, 1);

smp16ms  = 0.016384/2;    %   16ms
tic
for k = 1:(length(time) - 1)
    x_ol(:, k + 1) = x_ol(:, k) + smp16ms*fcn_MVEM(x_ol(:, k), u_ref(:, k), [speed_ref(k); fuel_ref(k)]);
    [~, y_ol(:, k + 1)] = fcn_MVEM(x_ol(:, k + 1), u_ref(:, k + 1), [speed_ref(k + 1); fuel_ref(k + 1)]);
end
toc


%% Generate Controllers at linearisation points

%Grid of linearisation points
speedlinpt = [800, 1600, 2400];
fuellinpt = [5, 30, 55, 80];
[speedlingrid, fuellingrid] = meshgrid(speedlinpt, fuellinpt);
Q =diag([0.01 1 0.01 1 0.01 1 0.01 0.2 0.1]);
% Q =diag([0.01 0.1]);
Cd = eye(9);
% Cd = [0 0 1 0 0 0 0 0 0;0 0 0 0 0 0 0 0 1];

y_ref = Cd * x_ref;

for i = 1:length(fuellinpt)
    for j = 1:length(speedlinpt)
        [Linv, F, J, c, W, A, B] = get_controller_from_opcon(speedlingrid(i, j), fuellingrid(i, j));
        controllers(i, j) = struct('Linv', Linv, 'F', F, 'J', J, 'c', c, 'W', W, 'A', A, 'B', B);
    end
end

%% Closed Loop MPC simulation

x_mpc = zeros(9, length(time));
y_mpc = zeros(9, length(time));
u_mpc = zeros(3, length(time));

x_mpc(:, 1) = x_ref(:, 1);
y_mpc(:, 1) = y_ref(:, 1);

%Options for MPC QP solver
options = mpcqpsolverOptions;
options.IntegrityChecks = false; %makes it faster but risks runtime errors

tic
for k = 1:(length(time) - 1-10)
    k
    %Switched controllers. If the linearisation points change, this code
    %needs to be changed
%     Get linear model should base on current State.
     [Adisc, Bdisc] = get_9state_linmodel(speed_ref(k), fuel_ref(k));
     sys = ss(Adisc,Bdisc,Cd,zeros(9,3),smp16ms) ;
     p=100;
     m=50;

     mpcobj = mpc(sys,smp16ms,p,m);
     mpcobj.Weights.OutputVariables =Q;
%      mpcobj.Weights.ManipulatedVariablesRate = [0.01 0.01 0.01];
     x = mpcstate(mpcobj);
%      stop printing;
    u = mpcmove(mpcobj, x,y_mpc(:, k ),y_ref(:, k+1:k+1+p)',zeros(2,1));
    if (speed_ref(k) <= 1200)
        j = 1;
    elseif (speed_ref(k) <= 2000)
        j = 2;
    else
        j = 3;
    end
    if (fuel_ref(k) <= 17.5)
        i = 1;
    elseif (fuel_ref(k) <= 42.5)
        i = 2;
    elseif (fuel_ref(k) <= 67.5)
        i = 3;
    else
        i = 4;
    end
    
%     Linv = controllers(i, j).Linv;
%     F = controllers(i, j).F;
%     J = controllers(i, j).J;
%     c = controllers(i, j).c;
%     W = controllers(i, j).W;
    
%     xtrim = -( x_ref(:, k)-x_mpc(:, k));
    
%     U_opt = mpcqpsolver(Linv, F*xtrim, -J, -c - W*xtrim, [], zeros(0,1), false(size(J, 1),1), options);
%     u_opt = U_opt(1:3);
    
%     u_mpc(:, k) = u_ref(:, k) + u_opt;
%     u_mpc(1, k) = max(min(u_mpc(1, k), 95), 0);
%     u_mpc(2, k) = max(min(u_mpc(2, k), 90), 0);
%     u_mpc(3, k) = max(min(u_mpc(3, k), 95), 20);
        
        x_mpc(:, k + 1) = x_mpc(:, k) + smp16ms*fcn_MVEM(x_mpc(:, k), u, [speed_ref(k); fuel_ref(k)]);
%     [~, y_mpc(:, k + 1)] = fcn_MVEM(x_mpc(:, k + 1), u, [speed_ref(k + 1); fuel_ref(k + 1)]);

        y_mpc(:, k + 1) =Cd*x_mpc(:, k + 1) ;
end
toc

figure(3)
% plot(time, y_ref(1, :))
hold on
plot(time, x_ol(3, :))
plot(time, x_mpc(3, :))
xlabel('Time (s)')
ylabel('Boost pressure (kPa)')
legend({'ref', 'MPC'})

figure(4)
% plot(time, y_ref(2, :))
hold on
plot(time, x_ol(9, :))
plot(time, x_ref(9, :))
xlabel('Time (s)')
ylabel('EGR Rate')
legend({ 'ref', 'MPC'})
weighting = diag([0.01 1 0.01 1 0.01 1 0.01 0.2 0.1]);