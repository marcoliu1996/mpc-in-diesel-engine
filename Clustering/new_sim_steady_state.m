function [x_bar, y_bar, x4_bar, converged] = new_sim_steady_state(u_bar, Ne_rpm, w_fuel,x_init)
%obtains the steady state values for given operating condition


converged = 0;

%max number of iterations
kmax = 10000;

%initial guess
x = x_init;

smp8ms   = 0.008192;    %    8ms
smp16ms  = 0.016384;    %   16ms

%sample time
%Note: choosing 16ms will give some unstable simulations for higher
%rpm/fuel
dt = smp8ms;

weighting = diag([0.01 1 0.01 1 0.01 1 0.01 0.2 0.1]);

for k = 1:kmax
    
    [dxdt, y, x4] = fcn_MVEM(x, u_bar, [Ne_rpm; w_fuel]);
    x_plus = x + dt*dxdt;
    
    err = sqrt((x_plus - x)'*weighting*(x_plus - x));
    if (err <= 10e-4)
        converged = 1;
        break
    elseif (any(isnan(x_plus)))
        break
    else
        x = x_plus;
    end
end

x_bar = x_plus;
[~, y_bar, x4_bar] = fcn_MVEM(x_bar, u_bar, [Ne_rpm; w_fuel]);