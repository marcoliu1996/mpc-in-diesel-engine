function [dxdt, y, x4] = fcn_MVEM(x, u, opcon)
%Date: 23/8/18
%Dynamics and output of the MVEM
%
%Function inputs:
%x: 9-dimensional state vector of [p_im; rho_im; p_em; rho_em; p_pth;
%rho_pth; p_turb; N_t; W_egr]
%u: 3-dimensional input vector of [u_thr; u_egr; u_vgt]
%opcon: 2-dimensional operating point of [engine_speed (rpm); fuel_rate
%(mm^3/str)]
%
%Function outputs:
%dxdt: 9-dimensional state derivative vector
%y: 2-dimensional output vector of [p_im; y_egr]
%x4: 4-dimensional state vector in reduced order model of [p_im; p_em;
%W_comp; y_egr]

p_out_intake_man = x(1);
rho_out_intake_man = x(2);
p_out_exhaust_man = x(3);
rho_out_exhaust_man = x(4);
p_out_preThrottle = x(5);
rho_out_preThrottle = x(6);
p_out_turbine_x = x(7);
omega_turbo = x(8);
W_egr_valve_x = x(9);
          
throttle_cmd = u(1);
valve_cmd = u(2);
VGT_cmd = u(3);

engine_speed_rpm = opcon(1);
engine_speed = opcon(1)*pi/30;
fuel_rate = opcon(2); %mm^3/str
W_fuel = 0.832/1000*2/(2*pi)*fuel_rate*engine_speed; %convert to g/s

%EGR bypass valve map
if (engine_speed_rpm <= 1000 && fuel_rate <= 25) || (engine_speed_rpm > 1000 && fuel_rate <= 5)
    bypass_mode = 1;
else
    bypass_mode = 3;
end

%------------------------------------------------------------
% Physical properties
%------------------------------------------------------------
gamma_air = 1.4;
gamma_exh = 1.34;
Flow_constant = gamma_air;
R_air = 287.0;          % J/kg-K
R_exh = 288.5;
Cp_air = 1009;          % J/kg-K
Cp_exh = 1134;
density_air = 1.161;    % kg/m^3 
density_fuel = 832e-9;  % kg/mm^3
qhv = 43e6;
AbF_st = 14.5;

%------------------------------------------------------------
% Environmental conditions
%------------------------------------------------------------
pcab = 0.9777e2;     % kPa
Tcab = 273.15+24.8;  % K
T_ambient = Tcab;    % K 
T_coolant = 360.65;  % K 

%------------------------------------------------------------
% Conversion factors
%------------------------------------------------------------
relP_to_absP = 100;
g_to_kg = 1000;
C_to_K = 273.15;               % convert to Kelvin
kPa_to_Pa = 1000;
deg_to_rad = pi/180; 

%------------------------------------------------------------
% Compressor
%------------------------------------------------------------
compressor_eff_cutoff = 0.03;
compressor_diameter = 49.6/1000;

%default parameters
compressor_flow_param = [239.498702088153,243.382206678789,-11.5163765178639,-198.604130666798,3.94924219206020,-514.524802094757,15.4745593931927,-102.816476234935,15.2495432485215,0.354889894652835,-1.68699689568380,-53.0223133728600,11.4611753164768,0.433560930984589];                   
compressor_temp_param = [210.054453284212,81.2920654973509,0.139726330925387,2.19575076485265,-0.447780602609118,0.00331083447213446,7.01412967617318]; 

%calibration parameters from Jan 2018 data
%compressor_flow_param = [383.304744656248;26.5212870268898;-1.43579604637643;-382.610525024939;-13.8509842168662;-14.0020211810557;0.539801541669884;-26.2865134624236;3.66740751891650;1.43149401021788;-1.65775181088591;-1.10695135783811;18.4908373277839;0.614648095679433]';
%compressor_temp_param = [205.409975413370;100.968705303540;0.329013571227540;0.646456233571351;-0.785906394985326;0.00848216041277449;4.38382661446562]';

%------------------------------------------------------------
% Intercooler
%------------------------------------------------------------
ic_temp_param = [-0.0489494055910931,-0.926699630949093,0.000343489445698112,0.0425488086106519]; 

%------------------------------------------------------------
% Pre throttle manifold
%------------------------------------------------------------
V_preThrottle = 3.115e-3*3;

%------------------------------------------------------------
% Throttle
%------------------------------------------------------------
%default parameters
Vth_coeff = [-2.01570914775653,0.184058742580999,-0.647680099040888,-3.29639729500754,0.0447750050096649];

%calibration parameters from Jan 2018 data
%Vth_coeff = [-7.98136639915200;0.180009337260979;-0.556619298640989;-2.13413089552706;0.0904649143294571]';

%------------------------------------------------------------
% Intake manifold
%------------------------------------------------------------
V_im = 2.7768e-3*3;

%------------------------------------------------------------
% Cylinder
%------------------------------------------------------------
displacement = 2.75e-3;        % m^3

%default parameters
cylinder_temp_param = [305.272060197684,-0.586770836006901,9440.76231196986,17.6443622494349,0.234550753617386,-15444.1855922193,0.00179938150508780,2.28521794104552,-0.00647009911439942];
cylinder_flow_param = [-0.00563633628353632,-0.00211378330475226,0.188356833658121,0.0444336647644923];

%calibration parameters from Jan 2018 data
%cylinder_temp_param = [398.909410349119;109.962541675149;36.1922949980725;810.644299735062;-0.322025157237953;-786.959519952704;-103.812318174759;-1.07507383372185;-0.00192167161045408]';
%cylinder_flow_param = [83236.4551665800,-187.903050238452,-5669.17122979610,1538.01413268572];

%------------------------------------------------------------
% Exhaust manifold
%------------------------------------------------------------
V_em = 1.91665e-3*5;

ht_exh = 0.0201251293189794;

%------------------------------------------------------------
% EGR cooler
%------------------------------------------------------------
%default parameters
egrc_temp_param  = [-3.54858174486326,0.981185189897968,1.40759984333328,0.000462988546558950,0.0214532297675437,-0.000292100807695256,-0.00205734616110405]; 
egrc_temp_param2 = [338.499692583367,0.221385316423523,0.0403590153390479,-0.00170664988053738];

%calibration parameters from Jan 2018 data
%egrc_temp_param = [1.43655255197009;0.973332800819760;-0.819211104440990;-0.00635523628738024;0.0444657193714359;7.61188543628047e-05;-3.83187344769682e-05]';
%egrc_temp_param2 = [376.201745255428;0.294203931086991;-0.00548866865868474;-1.70585023005704e-145]';

%------------------------------------------------------------
% EGR valve
%------------------------------------------------------------
P_ratio_crit = 1/(1+gamma_exh);
egrv_phi_crit = sqrt(gamma_exh/(2*(gamma_exh+1)));

egrv_phi_coeff  = [(1/2 - 1/2/gamma_exh) 1/gamma_exh -(1/2 + 1/2/gamma_exh)];
%default parameters
%egrv_flow_param = [0.676452415919464,-0.000403165997488821,0.0201748592582267]; 

%calibration parameters from Jan 2018 data
egrv_flow_param = [-5.12693325904818;0.00224676118854876;-0.000941667644818976]';

%------------------------------------------------------------
% EGR valve flow filter
%------------------------------------------------------------
% a_valve_act  = -40;
% b_valve_act  = 8;
% c_valve_act = 5;

%custom tuned parameters
b_valve_act  = 0.4;
a_valve_act  = -b_valve_act;
c_valve_act = 1;

%------------------------------------------------------------
% Turbine
%------------------------------------------------------------
%default parameters
turbine_flow_param = [22.1525643370717,1.72141939963304,-0.152554585820444,-9.74385037514348,2.13312773774300e-05]; 
turbine_temp_param = [-249.268069009752,411.290307867573,-184.926808818479,0.263748278556284]; 
turbine_press_param = [0.00245333949659976,-0.274115816537708,0.000355206161827237,98.8586109049417]; 

%calibration parameters from Jan 2018 data
%turbine_flow_param = [29.0515426686454;-4.54847952043559;-0.313668971217944;-3.81206839218819;0.00105479657822616]';

turbine_diameter = 0.0575;    
I_tc = 9.905e-6;

Cp_coef = [0.000102163169683323,0.174302954516597,965.119307961787]; 

%------------------------------------------------------------
% Turbine output pressure filter
%------------------------------------------------------------
% a_p_turb_fil = -40;
% b_p_turb_fil = 8;
% c_p_turb_fil = 1;

%custom tuned parameters
b_p_turb_fil  = 40;
a_p_turb_fil  = -b_p_turb_fil;
c_p_turb_fil = 1;


%% ========================================================================
% GD MVM V2P1
% By: Ken Butts, Huayi Li, Tony Kim, Sandesh Kirolkar
% Date: Feb 12, 2016
% GD MVM is calibrated using the TMC GD VDE model

% Derivatives Outputs:
% del_omega_turbo           - [krad/s]    
% del_p_out_exhaust_man     - [kPa] 
% del_p_out_intake_man      - [kPa] 
% del_p_out_preThrottle     - [kPa] 
% del_p_out_turbine_x       - [kPa] 
% del_rho_out_exhaust_man   - [kg/m3] 
% del_rho_out_intake_man    - [kg/m3] 
% del_rho_out_preThrottle   - [kg/m3] 
% del_W_egr_valve_x         - [g/s] 

% State variable:
% omega_turbo               - Turbine speed [krad/s] 
% p_out_exhaust_man         - Exhaust manifold pressure [kPa]
% p_out_intake_man          - Intake manifold pressure [kPa]
% p_out_preThrottle         - Pre-throttle manifold pressure [kPa]
% p_out_turbine_x           - Filtered turbine outlet pressure [kPa]
% rho_out_exhaust_man       - Exhaust manifold gas density [kg/m3]
% rho_out_intake_man        - Intake manifold gas density [kg/m3]
% rho_out_preThrottle       - pre throttle manifold  gas density [kg/m3]
% W_egr_valve_x             - Filtered EGR flow [g/s]


% Control Inputs:
% bypass_mode               - 2 and 3 = no bypass; 1 = bypass
% throttle_cmd              - throttle command [% closed]
% valve_cmd                 - valve command [% open]
% VGT_cmd                   - VGT command [% closed]

% Disturbance Inputs:
% engine_speed              - engine speed [rad/s]
% W_fuel                    - Total fuel flow [g/s]

% Intermediate variables:
% F_fuel
% p_out_turbine             - Turbine outlet pressure [Pa]
% p_out_turbine_filt        - Filtered turbine outlet pressure [Pa]
% T_out_comp                - Compressor outlet temperature [degK]
% T_out_EGR_cooler          - EGR cooler outlet temperature [degK]
% T_out_EGR_pipe            - EGR pipe outlet temperature [degK]
% T_out_intake_man          - Intake manifold temperature [degK]
% T_out_preThrottle         - Pre-throttle manifold temperature [degK]
% T_out_turbine             - Turbine outlet temperature [degK]
% W_comp                    - Compressor flow [g/s]
% W_egr_throttle            - EGR throttle flow [g/s]
% W_egr_valve_filt          - Filtered EGR valve flow [g/s]
% W_in_cyl                  - Total cylinder in flow [g/s]
% W_out_cyl                 - Total cylinder out flow [g/s]
% W_turbine                 - Turbine flow [g/s]

%% ========================================================================
% System-level Tuning

if engine_speed < 100
    eff_egrt_param      = [-4.58117680180854,0.118580252502370,0.221329806207997,-0.00133541371192224,-0.000632514796820491,-0.0599008212633061];  
    eff_turbine_param   = [-4.50963846607371,0.119618723275681,1.02408353926812,-0.0291489547501942,-0.000595640401331718,1.14015419907239];
elseif engine_speed < 150
    eff_egrt_param      = [0.941605291832053,0.00203238464729628,-0.137344381267610,0.00251509681003580,-2.19088213275662e-05,-0.0543872255022791];
    eff_turbine_param   = [0.463879386098971,0.00622726628733759,0.194846010031586,-0.00143597162457423,-2.01384922570834e-05,0.0316974327176144];
elseif engine_speed < 200
    eff_egrt_param      = [0.793093318928246,0.00246351053886058,-0.0448766207220591,0.00104906792049749,-1.34557575181562e-05,-0.0240798310348999];
    eff_turbine_param   = [6.26471255337321,-0.0646719600999177,0.247780565438478,-0.00103180425575771,0.000191621965696083,0.00466326961495468];
elseif engine_speed < 300
    eff_egrt_param      = [0.273939096576857,0.00320918980499703,0.150119257225737,-0.000227147995915401,-3.52583652276596e-06,-0.0115889058336661];
    eff_turbine_param   = [4.71205957606078,-0.0300272497964544,0.0932085962468785,0.000771363130301694,5.11814675217349e-05,-0.0309333322740195];
else
    eff_egrt_param      = [2.08049768011780,-0.00569218777154623,-0.00984393567945287,0.000138476439525508,6.79291619275290e-06,-0.00401381359875760];
    eff_turbine_param   = [0.822098674760177,-0.00143047580824002,0.185668176651382,-0.000158512076348171,1.75859970028700e-06,-0.0101990952904857];
end

%% =========================================================================
% Update filtered variables

W_egr_valve_filt        = c_valve_act*W_egr_valve_x;       %Filter used to avoid an algebraic loop
p_out_turbine_filt      = c_p_turb_fil*p_out_turbine_x;    %Filter used to avoid an algebraic loop

%% ========================================================================
% Compressor
% Output: W_comp [g/s], T_out_comp [K]

P_ratio_comp            = p_out_preThrottle/pcab;

engine_speed_reduced    = engine_speed/1000;

W_comp                  = compressor_flow_param(1) + compressor_flow_param(2).*engine_speed_reduced + compressor_flow_param(3).*omega_turbo + compressor_flow_param(4).*P_ratio_comp + ...
                          compressor_flow_param(5).*W_fuel + compressor_flow_param(6).*engine_speed_reduced.^2 + compressor_flow_param(7).*engine_speed_reduced.*omega_turbo + ...
                          compressor_flow_param(8).*engine_speed_reduced.*P_ratio_comp + compressor_flow_param(9).*omega_turbo.*P_ratio_comp + compressor_flow_param(10)*omega_turbo.^2 + compressor_flow_param(11).*W_fuel.*omega_turbo + ...
                          compressor_flow_param(12).*P_ratio_comp.^2 + compressor_flow_param(13).*P_ratio_comp.*W_fuel + compressor_flow_param(14).*W_fuel.^2;

%softplus
W_comp = log(1 + exp(2*W_comp));                
                      
T_out_comp              = compressor_temp_param(1) + compressor_temp_param(2).*P_ratio_comp + compressor_temp_param(3).*W_comp+ compressor_temp_param(4).*omega_turbo + ...
                          compressor_temp_param(5).*W_comp.*P_ratio_comp + compressor_temp_param(6)*W_comp.^2 + compressor_temp_param(7).*P_ratio_comp.^2;
                      
                      
%% ========================================================================
% Pre Throttle Manifold
% Output: T_out_preThrottle [K]

T_out_preThrottle       = 1000*p_out_preThrottle/(rho_out_preThrottle*R_air);

%% ========================================================================
% EGR Throttle
% Output: W_egr_throttle [g/s]
% Ref: Jiang, Li, et al. "Parameterization and simulation for a turbocharged spark ignition direct injection engine with variable valve timing." SAE paper 2009-01 (2009): 0680.

V_th_Lps                = Vth_coeff(1) + Vth_coeff(2)*engine_speed + Vth_coeff(3)*W_egr_valve_filt + Vth_coeff(4)*W_egr_valve_filt/(86-throttle_cmd*86/100) + Vth_coeff(5)*(86-throttle_cmd*86/100);
W_egr_throttle          = p_out_preThrottle*10^3/(R_air*T_out_preThrottle)*V_th_Lps/1000*10^3;

if W_fuel < 0.16
    W_egr_throttle      = W_egr_throttle + 2.1;      %EGR throttle flow offset for idling conditions
end

theta_EGRT              = eff_egrt_param(1) + eff_egrt_param(2).*engine_speed + eff_egrt_param(3).*W_fuel + eff_egrt_param(4).*engine_speed.*W_fuel +  eff_egrt_param(5).*engine_speed.^2 + ...
                          eff_egrt_param(6).*W_fuel.^2;
                      
W_egr_throttle          = theta_EGRT*W_egr_throttle;

%% ========================================================================
% Intake Manifold
% Output: T_out_intake_man [K]

T_out_intake_man       = 1000*p_out_intake_man/(rho_out_intake_man*R_air);

%% ========================================================================
% Cylinder (Cylinder torque calculation omitted)
% Output: T_out_cyl [K], W_out_cyl [g/s]


vol_eff_model           = cylinder_flow_param(1)*sqrt(max(0, p_out_intake_man)) + cylinder_flow_param(2)*sqrt(max(0, engine_speed)) + ...
                          cylinder_flow_param(3)+ cylinder_flow_param(4).*sqrt(max(T_out_intake_man, 0));

W_in_cyl                = 1000*vol_eff_model*(p_out_intake_man*1000)*engine_speed*(60/(2*pi))*displacement/(120*R_air*T_out_intake_man); 
W_out_cyl               = W_fuel + W_in_cyl; 
F_fuel                  = W_fuel/(W_out_cyl);

T_out_cyl               = cylinder_temp_param(1) + cylinder_temp_param(2)*W_in_cyl + cylinder_temp_param(3)*F_fuel + cylinder_temp_param(4)*F_fuel*W_in_cyl + ...
                          cylinder_temp_param(5)*engine_speed + cylinder_temp_param(6)*F_fuel.^2 + cylinder_temp_param(7)*W_in_cyl^2 + ...
                          cylinder_temp_param(8)*W_egr_valve_filt + cylinder_temp_param(9)*W_egr_valve_filt^2;
           
%% ========================================================================
% Exhaust manifold
% Output: T_out_exhaust_man [kPa], Cp_exh_model [J/kg-K]

T_out_exhaust_man       = 1000*p_out_exhaust_man/(rho_out_exhaust_man*R_air);

Cp_exh_model            = Cp_coef(1)*T_out_cyl^2 + Cp_coef(2)*T_out_cyl + Cp_coef(3);

%% ========================================================================
% Turbine Flow
% Output: W_turbine [g/s], T_out_turbine [K], p_out_turbine [kPa]

P_ratio_tur_em          = p_out_turbine_filt/p_out_exhaust_man;

phi_t_model             = turbine_flow_param(1) + turbine_flow_param(2).*P_ratio_tur_em + turbine_flow_param(3).*VGT_cmd + turbine_flow_param(4).*P_ratio_tur_em.^2 + turbine_flow_param(5).*VGT_cmd.^2 ;

W_turbine               = phi_t_model.*p_out_exhaust_man./sqrt(max(0, T_out_exhaust_man)); 

% Turbine temperature out  
T_out_turbine           = T_out_exhaust_man + (turbine_temp_param(1)  + turbine_temp_param(2)*P_ratio_tur_em + turbine_temp_param(3)*P_ratio_tur_em.^2 +  turbine_temp_param(4)*VGT_cmd );

% Turbine pressure out
p_out_turbine           = turbine_press_param(1).*W_turbine.^2 + turbine_press_param(2).*W_turbine + turbine_press_param(3).*T_out_turbine.*W_turbine + turbine_press_param(4);

% Turbine power balance
fac = 1;

theta_T                 = eff_turbine_param(1) + eff_turbine_param(2).*engine_speed + eff_turbine_param(3).*W_fuel + eff_turbine_param(4).*engine_speed.*W_fuel +  eff_turbine_param(5).*engine_speed.^2 + ...
                          eff_turbine_param(6).*W_fuel.^2;

Pow_turbine             = ((Cp_exh_model/1000)*W_turbine*(T_out_exhaust_man-T_out_turbine));
Pow_turbine             = theta_T*Pow_turbine;

Pow_comp                = ((Cp_air/1000)*W_comp*(T_out_comp-Tcab));
net_Pow                 = fac*((Pow_turbine) - (Pow_comp));

    
%% ========================================================================
% EGR cooler
% Output: T_out_EGR_cooler [K]

delTc                   = T_coolant - T_out_exhaust_man;
delTc_model             = egrc_temp_param(1) + egrc_temp_param(2)*delTc + egrc_temp_param(3)*W_egr_valve_filt + egrc_temp_param(4)*delTc*W_egr_valve_filt + egrc_temp_param(5)*W_egr_valve_filt^2 + ...
                          egrc_temp_param(6)*delTc.*W_egr_valve_filt^2 + egrc_temp_param(7)*W_egr_valve_filt^3;
                    
T_out_EGR_cooler        = T_out_exhaust_man + delTc_model;
    if (bypass_mode < 1.5)
        delTc            = T_out_exhaust_man - Tcab;
        T_out_EGR_cooler = egrc_temp_param2(1) + egrc_temp_param2(2).*delTc + egrc_temp_param2(3).*delTc.*W_egr_valve_filt + egrc_temp_param2(4).*p_out_exhaust_man.*(W_egr_valve_filt.^2);
    end
T_out_EGR_cooler = max(T_out_EGR_cooler, T_coolant);

%% ========================================================================
% EGR valve
% Output: static_valve_flow [g/s]

ratio_model             = egrv_flow_param(1)+ egrv_flow_param(2)*valve_cmd.*engine_speed + egrv_flow_param(3)*valve_cmd.^2;
cor                     = sqrt(max(0, (p_out_exhaust_man.*p_out_exhaust_man.*R_air.*Tcab)./(T_out_EGR_cooler.*R_exh.*pcab.*pcab)));
P_ratio_im_em           = p_out_intake_man/p_out_exhaust_man;

if P_ratio_im_em < P_ratio_crit
    phi_egrv = egrv_phi_crit;
elseif P_ratio_im_em >= P_ratio_crit && P_ratio_im_em <= 1
    phi_egrv = sqrt(max(0, egrv_phi_coeff(1) + egrv_phi_coeff(2)*(P_ratio_im_em) + egrv_phi_coeff(3)*(P_ratio_im_em).^2));
else % allow reverse flow
    phi_egrv = -1*sqrt(max(0, egrv_phi_coeff(1) + egrv_phi_coeff(2)*(1/P_ratio_im_em) + egrv_phi_coeff(3)*(1/P_ratio_im_em).^2));
end

static_valve_flow = ratio_model.*phi_egrv.*cor;

%softplus
static_valve_flow = log(1 + exp(2*static_valve_flow));

%% ========================================================================
% Calculate the derivatives

%integrity check first
integrity_checks = 0;
if (integrity_checks)
    if isnan(W_comp); disp('W_comp NaN'); end
    if isnan(T_out_preThrottle); disp('T_out_preThrottle NaN'); end
    if isnan(W_egr_throttle); disp('W_egr_throttle NaN'); end
    if isnan(W_egr_valve_filt); disp('W_egr_valve_filt NaN'); end
    if isnan(T_out_EGR_cooler); disp('T_out_EGR_cooler NaN'); end
    if isnan(T_out_intake_man); disp('T_out_preThrottle NaN'); end
    if isnan(p_out_turbine); disp('p_out_turbine NaN'); end
    if isnan(static_valve_flow); disp('static_valve_flow NaN'); end
    if isnan(W_out_cyl); disp('W_out_cyl NaN'); end
    if isnan(T_out_cyl); disp('T_out_cyl NaN'); end
    if isnan(net_Pow); disp('net_Pow NaN'); end
    if isnan(W_in_cyl); disp('W_in_cyl NaN'); end
end


del_p_out_preThrottle   = ((W_comp*T_out_preThrottle)-(W_egr_throttle*T_out_preThrottle))*gamma_air*R_air/(V_preThrottle*10^6); %[kPa]
del_rho_out_preThrottle = (W_comp-W_egr_throttle)/(V_preThrottle*1000); % [kg/m3]

del_p_out_intake_man    = ((W_egr_throttle*T_out_preThrottle)+(W_egr_valve_filt*T_out_EGR_cooler)-(W_in_cyl*T_out_intake_man))*gamma_air*R_air/(V_im*10^6); %[kPa]
del_rho_out_intake_man  = (W_egr_throttle+W_egr_valve_filt-W_in_cyl)/(V_im*1000); % [kg/m3]

del_p_out_exhaust_man   = ((W_out_cyl*T_out_cyl)-(W_egr_valve_filt*T_out_exhaust_man)-(W_turbine*T_out_exhaust_man) ... 
                            - (((T_out_cyl-Tcab).*W_out_cyl.*ht_exh)/Cp_exh_model))*gamma_air*R_air/(V_em*10^6); % [kPa]
del_rho_out_exhaust_man = (W_out_cyl-W_egr_valve_filt-W_turbine)/(V_em*1000);  % [kg/m3]

del_omega_turbo         = net_Pow/(omega_turbo*I_tc*1000000); % [krad/s]
del_p_out_turbine_x     = a_p_turb_fil*p_out_turbine_x + b_p_turb_fil*p_out_turbine; %[kPa]
del_W_egr_valve_x       = a_valve_act*W_egr_valve_x + b_valve_act*static_valve_flow; %[g/s]


dxdt = [del_p_out_intake_man; del_rho_out_intake_man; del_p_out_exhaust_man;
    del_rho_out_exhaust_man; del_p_out_preThrottle; del_rho_out_preThrottle;
    del_p_out_turbine_x; del_omega_turbo; del_W_egr_valve_x];

%%

y_egr = W_egr_valve_filt/(W_egr_valve_filt + W_comp);
y = [p_out_intake_man; y_egr];

x4 = [x(1); x(3); W_comp; y_egr];

