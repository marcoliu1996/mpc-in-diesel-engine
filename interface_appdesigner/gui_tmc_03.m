function varargout = gui_tmc_03(varargin)
% GUI_TMC_03 MATLAB code for gui_tmc_03.fig
%      GUI_TMC_03, by itself, creates a new GUI_TMC_03 or raises the existing
%      singleton*.
%
%      H = GUI_TMC_03 returns the handle to a new GUI_TMC_03 or the handle to
%      the existing singleton*.
%
%      GUI_TMC_03('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TMC_03.M with the given input arguments.
%
%      GUI_TMC_03('Property','Value',...) creates a new GUI_TMC_03 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_tmc_03_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_tmc_03_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_tmc_03

% Last Modified by GUIDE v2.5 31-Aug-2017 17:28:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_tmc_03_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_tmc_03_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui_tmc_03 is made visible.
function gui_tmc_03_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_tmc_03 (see VARARGIN)

%% % Oprating points panel related variables
%% % Engine speed 
% %% Points
% %%
engine_speed_points = [800, 1600, 2400];
handles.engine_speed_points = engine_speed_points;
% %% Iteratorator range
% %%
[~, iterator_range__engine_speed_points] = size(engine_speed_points);
handles.iterator_range__engine_speed_points = iterator_range__engine_speed_points;
%% % Fuel rate 
% %% Final points
% %%
fuel_rate_final_points = [80, 55, 30, 5];
handles.fuel_rate_final_points = fuel_rate_final_points;
% %% Final points iterator range
% %%
[~, iterator_range__fuel_rate_final_points] = size(fuel_rate_final_points);
handles.iterator_range__fuel_rate_final_points = iterator_range__fuel_rate_final_points;
% %% Initial points 1
% %%
fuel_rate_initial_points1 = [50, 80, 70, 35];
handles.fuel_rate_initial_points1 = fuel_rate_initial_points1;
% %% Initial points 1 iterator range
% %%
[~, iterator_range__fuel_rate_initial_points1] = size(fuel_rate_initial_points1);
handles.iterator_range__fuel_rate_initial_points1 = iterator_range__fuel_rate_initial_points1;
% %% Initial points 2
% %%
fuel_rate_initial_points2 = [40, 15, 2.500000e+00, 45];
handles.fuel_rate_initial_points2 = fuel_rate_initial_points2;
% %% Initial points 2 iterator range
% %%
[~, iterator_range__fuel_rate_initial_points2] = size(fuel_rate_initial_points2);
handles.iterator_range__fuel_rate_initial_points2 = iterator_range__fuel_rate_initial_points2;
%% % Data trajectories
% %% Loading trajectory data - this data is obtained by running MPC and MVEM
% %% in a closed loop and extracting **boost pressure** and **egr rate**
% %% trajectoris by saving it in a file 'data_iterators.mat'.
% %%
trajectory_data__temp = load('trajectory_data.mat');
trajectory_data = trajectory_data__temp.trajectory_data_new;
handles.trajectory_data = trajectory_data;
%% % Declaring a number of trajectories taken per operating point
% %% 
number__trajecotries = 9;
handles.number__trajecotries = number__trajecotries;
%% % Declaring a number of total trajectories 
% %%
number__total_trajectories = 216;
handles.number__total_trajectories = number__total_trajectories;
%% % Loading previous grading progress
% %% Each time gui is being executed, it checks if any trajectories have been
% %% already graded (labeled) and if so it loads the corresponding data. This enables
% %% the user to resume the previous work. On the other hand, if none of the
% %% trajectories have been graded upon gui execution, it starts with an empty
% %% corresponding data structure.
% %%
if exist('labeled_data.mat') == 2
    %% % Load already graded (labeled) data
    % %%
    labeled_data__temp = load('labeled_data.mat');
    labeled_data = labeled_data__temp.labeled_data;
    %% % Forcing expert to pick a point from which he/she wishes to load trajectories
    % %%
    set(handles.pushbutton__grade, 'Enable', 'off');
    set(handles.pushbutton__go_back, 'Enable', 'off');
    set(handles.pushbutton__load, 'Enable', 'off');
    set(handles.pushbutton__export_data, 'Enable', 'off');
    %% % Setting the iterator total labeled to the number reached in the previous grading
    % %%
    handles.iterator__total_labeled = labeled_data{4};
    %% % Disabling points which are already labeled
    %% % First row (see documentation)
    % %% Point (800, 80, 50)
    % %%
    if labeled_data{1}{1}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
    end
    % %% Point (800, 80, 40)
    % %%
    if labeled_data{1}{1}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
    end
    % %% Point (1600, 80, 50)
    % %%
    if labeled_data{2}{1}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
    end
    % %% Point (1600, 80, 40)
    % %%
    if labeled_data{2}{1}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
    end
    % %% Point (2400, 80, 50)
    % %%
    if labeled_data{3}{1}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off');
    end
    % %% Point (2400, 80, 40)
    % %%
    if labeled_data{3}{1}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
    end
    %% % Second row (see documentation)
    % %% Point (800, 55, 80)
    % %%
    if labeled_data{1}{2}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
    end
    % %% Point (800, 55, 15)
    % %%
    if labeled_data{1}{2}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
    end
    % %% Point (1600, 55, 80)
    % %%
    if labeled_data{2}{2}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
    end
    % %% Point (1600, 55, 15)
    % %%
    if labeled_data{2}{2}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
    end
    % %% Point (2400, 55, 80)
    % %%
    if labeled_data{3}{2}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
    end
    % %% Point (2400, 55, 15)
    % %%
    if labeled_data{3}{2}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
    end
    %% % Third row (see documentation)
    % %% Point (800, 30, 70)
    % %%
    if labeled_data{1}{3}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
    end
    % %% Point (800, 30, 2.5)
    % %%
    if labeled_data{1}{3}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
    end
    % %% Point (1600, 30, 70)
    % %%
    if labeled_data{2}{3}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
    end
    % %% Point (1600, 30, 2.5)
    % %%
    if labeled_data{2}{3}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
    end
    % %% Point (2400, 30, 70)
    % %%
    if labeled_data{3}{3}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
    end
    % %% Point (2400, 30, 2.5)
    % %%
    if labeled_data{3}{3}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
    end
    %% % Fourth row (see documentation)
    % %% Point (800, 5, 35)
    % %%
    if labeled_data{1}{4}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
    end
    % %% Point (800, 5, 45)
    % %%
    if labeled_data{1}{4}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
    end
    % %% Point (1600, 5, 35)
    % %%
    if labeled_data{2}{4}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
    end
    % %% Point (1600, 5, 45)
    % %%
    if labeled_data{2}{4}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
    end
    % %% Point (2400, 5, 35)
    % %%
    if labeled_data{3}{4}{1}{2} == handles.number__trajecotries
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
    end
    % %% Point (2400, 5, 45)
    % %%
    if labeled_data{3}{4}{2}{2} == handles.number__trajecotries
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    end
    %% % Setting counters for each individual point to a value reached in the last grading (labeling) session
    %% % First row (see documentation)
    % %% Point (800, 80, 50)
    % %%
    set(handles.pushbutton__es800__fr80__fri50__cntr, 'String', strcat(num2str(labeled_data{1}{1}{1}{2}),'/', num2str(handles.number__trajecotries)));
    % %% Point (800, 80, 40)
    % %%
    set(handles.pushbutton__es800__fr80__fri40__cntr, 'String', strcat(num2str(labeled_data{1}{1}{2}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (1600, 80, 50)
    % %%
    set(handles.pushbutton__es1600__fr80__fri50__cntr, 'String', strcat(num2str(labeled_data{2}{1}{1}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (1600, 80, 40)
    % %%
    set(handles.pushbutton__es1600__fr80__fri40__cntr, 'String', strcat(num2str(labeled_data{2}{1}{2}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (2400, 80, 50)
    % %%
    set(handles.pushbutton__es2400__fr80__fri50__cntr, 'String', strcat(num2str(labeled_data{3}{1}{1}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (2400, 80, 40)
    % %%
    set(handles.pushbutton__es2400__fr80__fri40__cntr, 'String', strcat(num2str(labeled_data{3}{1}{2}{2}), '/', num2str(handles.number__trajecotries)));
    %% % Second row (see documentation)
    % %% Point (800, 55, 80)
    % %%
    set(handles.pushbutton__es800__fr55__fri80__cntr, 'String', strcat(num2str(labeled_data{1}{2}{1}{2}),'/', num2str(handles.number__trajecotries)));
    % %% Point (800, 55, 15)
    % %%
    set(handles.pushbutton__es800__fr55__fri15__cntr, 'String', strcat(num2str(labeled_data{1}{2}{2}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (1600, 55, 80)
    % %%
    set(handles.pushbutton__es1600__fr55__fri80__cntr, 'String', strcat(num2str(labeled_data{2}{2}{1}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (1600, 55, 15)
    % %%
    set(handles.pushbutton__es1600__fr55__fri15__cntr, 'String', strcat(num2str(labeled_data{2}{2}{2}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (2400, 55, 80)
    % %%
    set(handles.pushbutton__es2400__fr55__fri80__cntr, 'String', strcat(num2str(labeled_data{3}{2}{1}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (2400, 55, 15)
    % %%
    set(handles.pushbutton__es2400__fr55__fri15__cntr, 'String', strcat(num2str(labeled_data{3}{2}{2}{2}), '/', num2str(handles.number__trajecotries)));
    %% % Third row (see documentation)
    % %% Point (800, 30, 70)
    % %%
    set(handles.pushbutton__es800__fr30__fri70__cntr, 'String', strcat(num2str(labeled_data{1}{3}{1}{2}),'/', num2str(handles.number__trajecotries)));
    % %% Point (800, 30, 2.5)
    % %%
    set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'String', strcat(num2str(labeled_data{1}{3}{2}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (1600, 30, 70)
    % %%
    set(handles.pushbutton__es1600__fr30__fri70__cntr, 'String', strcat(num2str(labeled_data{2}{3}{1}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (1600, 30, 2.5)
    % %%
    set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'String', strcat(num2str(labeled_data{2}{3}{2}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (2400, 30, 70)
    % %%
    set(handles.pushbutton__es2400__fr30__fri70__cntr, 'String', strcat(num2str(labeled_data{3}{3}{1}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (2400, 30, 2.5)
    % %%
    set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'String', strcat(num2str(labeled_data{3}{3}{2}{2}), '/', num2str(handles.number__trajecotries)));
    %% % Fourth row (see documentation)
    % %% Point (800, 5, 35)
    % %%
    set(handles.pushbutton__es800__fr5__fri35__cntr, 'String', strcat(num2str(labeled_data{1}{4}{1}{2}),'/', num2str(handles.number__trajecotries)));
    % %% Point (800, 5, 45)
    % %%
    set(handles.pushbutton__es800__fr5__fri45__cntr, 'String', strcat(num2str(labeled_data{1}{4}{2}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (1600, 5, 35)
    % %%
    set(handles.pushbutton__es1600__fr5__fri35__cntr, 'String', strcat(num2str(labeled_data{2}{4}{1}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (1600, 5, 45)
    % %%
    set(handles.pushbutton__es1600__fr5__fri45__cntr, 'String', strcat(num2str(labeled_data{2}{4}{2}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (2400, 5, 35)
    % %%
    set(handles.pushbutton__es2400__fr5__fri35__cntr, 'String', strcat(num2str(labeled_data{3}{4}{1}{2}), '/', num2str(handles.number__trajecotries)));
    % %% Point (2400, 5, 45)
    % %%
    set(handles.pushbutton__es2400__fr5__fri45__cntr, 'String', strcat(num2str(labeled_data{3}{4}{2}{2}), '/', num2str(handles.number__trajecotries)));
else
    %% % Forcing expert to pick a point and from which he/she wishes to load trajectories
    % %%
    set(handles.pushbutton__grade, 'Enable', 'off');
    set(handles.pushbutton__go_back, 'Enable', 'off');
    set(handles.pushbutton__load, 'Enable', 'off');
    set(handles.pushbutton__export_data, 'Enable', 'off');
    %% % Setting "point selected" to empty
    % %%
    point_selected = '';
    handles.point_selected = point_selected;
    %% % Setting operating point iterators
    % %% Engine speed
    % %%
    iterator__engine_speed_points = 0;
    handles.iterator__engine_speed_points = iterator__engine_speed_points;
    % %% Fuel rate final
    % %%
    iterator__fuel_rate_final_points = 0;
    handles.iterator__fuel_rate_final_points = iterator__fuel_rate_final_points;
    % %% Fuel rate inital ponit 1
    % %%
    iterator__fuel_rate_initial_points1 = 0;
    handles.iterator__fuel_rate_initial_points1 = iterator__fuel_rate_initial_points1;
    % %% Fuel rate initial point 2
    % %%
    iterator__fuel_rate_initial_points2 = 0;
    handles.iterator__fuel_rate_initial_points2 = iterator__fuel_rate_initial_points2;
    % %% To indicate if selected fuel rate initial point is 1 or 2
    
    indicator__fuel_rate_initial_points = 0;
    handles.indicator__fuel_rate_initial_points = indicator__fuel_rate_initial_points;
    %% % "Initializing" grading data structure and its iterators
    %% % First row (see documentation)
    % %%
    labeled_data{1}{1}{1}{1} = zeros(1, handles.number__trajecotries); % Point (800, 80, 50, #trajectories = 9)
    labeled_data{1}{1}{1}{2} = 0; % Point (800, 80, 50, #trajectories = 9) iterator
    labeled_data{1}{1}{2}{1} = zeros(1, handles.number__trajecotries); % Point (800, 80, 40, #trajectories = 9)
    labeled_data{1}{1}{2}{2} = 0; % Point (800, 80, 40, #trajectories = 9) iterator
    labeled_data{2}{1}{1}{1} = zeros(1, handles.number__trajecotries); % Point (1600, 80, 50, #trajectories = 9)
    labeled_data{2}{1}{1}{2} = 0; % Point (1600, 80, 50, #trajectories = 9) iterator 
    labeled_data{2}{1}{2}{1} = zeros(1, handles.number__trajecotries); % Point (1600, 80, 40, #trajectories = 9)
    labeled_data{2}{1}{2}{2} = 0; % Point (1600, 80, 40, #trajectories = 9) iterator
    labeled_data{3}{1}{1}{1} = zeros(1, handles.number__trajecotries); % Point (2400, 80, 50, #trajectories = 9)
    labeled_data{3}{1}{1}{2} = 0; % Point (2400, 80, 50, #trajectories = 9) iterator
    labeled_data{3}{1}{2}{1} = zeros(1, handles.number__trajecotries); % Point (2400, 80, 40, #trajectories = 9)
    labeled_data{3}{1}{2}{2} = 0; % Point (2400, 80, 40, #trajectories = 9) iterator
    %% % Second row (see documentation)
    % %%
    labeled_data{1}{2}{1}{1} = zeros(1, handles.number__trajecotries); % Point (800, 55, 80, #trajectories = 9)
    labeled_data{1}{2}{1}{2} = 0; % Point (800, 55, 80, #trajectories = 9) iterator
    labeled_data{1}{2}{2}{1} = zeros(1, handles.number__trajecotries); % Point (800, 55, 15, #trajectories = 9)
    labeled_data{1}{2}{2}{2} = 0; % Point (800, 55, 15, #trajectories = 9) iterator
    labeled_data{2}{2}{1}{1} = zeros(1, handles.number__trajecotries); % Point (1600, 55, 80, #trajectories = 9)
    labeled_data{2}{2}{1}{2} = 0; % Point (1600, 55, 80, #trajectories = 9) iterator
    labeled_data{2}{2}{2}{1} = zeros(1, handles.number__trajecotries); % Point (1600, 55, 15, #trajectories = 9)
    labeled_data{2}{2}{2}{2} = 0; % Point (1600, 55, 15, #trajectories = 9) iterator
    labeled_data{3}{2}{1}{1} = zeros(1, handles.number__trajecotries); % Point (2400, 55, 80, #trajectories = 9)
    labeled_data{3}{2}{1}{2} = 0; % Point (2400, 55, 80, #trajectories = 9) iterator
    labeled_data{3}{2}{2}{1} = zeros(1, handles.number__trajecotries); % Point (2400, 55, 15, #trajectories = 9)
    labeled_data{3}{2}{2}{2} = 0; % Point (2400, 55, 15, #trajectories = 9) iterator
    %% % Third row (see documentation)
    % %%
    labeled_data{1}{3}{1}{1} = zeros(1, handles.number__trajecotries); % Point (800, 30, 70, #trajectories = 9)
    labeled_data{1}{3}{1}{2} = 0; % Point (800, 30, 70, #trajectories = 9) iterator
    labeled_data{1}{3}{2}{1} = zeros(1, handles.number__trajecotries); % Point (800, 30, 2.5, #trajectories = 9)
    labeled_data{1}{3}{2}{2} = 0; % Point (800, 30, 2.5, #trajectories = 9) iterator
    labeled_data{2}{3}{1}{1} = zeros(1, handles.number__trajecotries); % Point (1600, 30, 70, #trajectories = 9)
    labeled_data{2}{3}{1}{2} = 0; % Point (1600, 30, 70, #trajectories = 9) iterator
    labeled_data{2}{3}{2}{1} = zeros(1, handles.number__trajecotries); % Point (1600, 30, 2.5, #trajectories = 9)
    labeled_data{2}{3}{2}{2} = 0; % Point (1600, 30, 2.5, #trajectories = 9) iterator
    labeled_data{3}{3}{1}{1} = zeros(1, handles.number__trajecotries); % Point (2400, 30, 70, #trajectories = 9)
    labeled_data{3}{3}{1}{2} = 0; % Point (2400, 30, 70, #trajectories = 9) iterator
    labeled_data{3}{3}{2}{1} = zeros(1, handles.number__trajecotries); % Point (2400, 30, 2.5, #trajectories = 9)
    labeled_data{3}{3}{2}{2} = 0; % Point (2400, 30, 2.5, #trajectories = 9) iterator
    %% % Fourth row (see documentation)
    % %%
    labeled_data{1}{4}{1}{1} = zeros(1, handles.number__trajecotries); % Point (800, 5, 35, #trajectories = 9)
    labeled_data{1}{4}{1}{2} = 0; % Point (800, 5, 35, #trajectories = 9) iterator
    labeled_data{1}{4}{2}{1} = zeros(1, handles.number__trajecotries); % Point (800, 5, 45, #trajectories = 9)
    labeled_data{1}{4}{2}{2} = 0; % Point (800, 5, 45, #trajectories = 9) iterator
    labeled_data{2}{4}{1}{1} = zeros(1, handles.number__trajecotries); % Point (1600, 5, 35, #trajectories = 9)
    labeled_data{2}{4}{1}{2} = 0; % Point (1600, 5, 35, #trajectories = 9) iterator
    labeled_data{2}{4}{2}{1} = zeros(1, handles.number__trajecotries); % Point (1600, 5, 45, #trajectories = 9)
    labeled_data{2}{4}{2}{2} = 0; % Point (1600, 5, 35, #trajectories = 9) iterator
    labeled_data{3}{4}{1}{1} = zeros(1, handles.number__trajecotries); % Point (2400, 5, 35, #trajectories = 9)
    labeled_data{3}{4}{1}{2} = 0; % Point (2400, 5, 35, #trajectories = 9)
    labeled_data{3}{4}{2}{1} = zeros(1, handles.number__trajecotries); % Point (2400, 5, 45, #trajectories = 9)
    labeled_data{3}{4}{2}{2} = 0; % Point (2400, 5, 45, #trajectories = 9) itertor
    %% % Iterator for keeping track of total number of graded (labeled) trajectories
    % %%
    iterator__total_labeled = 0; 
    handles.iterator__total_labeled = iterator__total_labeled;
    % %% Ad-hock inlcusion of the "iterator__total_labeled" into the data
    % %% structure 
    labeled_data{4} = [0];
end
% %% Declaring handles "labeled data" outside "if statement" because it can be
% %% either the empty data structure or a data structure used before
% %%
handles.labeled_data = labeled_data;
% %% Declaring a switch between initial fuel rate 1 and 2
% %%
handles.switch__collection_labeled = 0;
% %% Declaring default axes Y labels
% %%
axes(handles.axes__egr_rate);
set(get(handles.axes__boost_pressure, 'YLabel'), 'String', 'Boost pressure (kPa)');
set(get(handles.axes__egr_rate, 'YLabel'), 'String', 'EGR rate (%)');
% %% Disabeling garding radio buttonw a best/worst radio button is selected
% %%
set(handles.radiobutton__best_0, 'Enable', 'off');
set(handles.radiobutton__best_20, 'Enable', 'off');
set(handles.radiobutton__best_40, 'Enable', 'off');
set(handles.radiobutton__best_60, 'Enable', 'off');
set(handles.radiobutton__best_80, 'Enable', 'off');
set(handles.radiobutton__best_100, 'Enable', 'off');
set(handles.radiobutton__worst_0, 'Enable', 'off');
set(handles.radiobutton__worst_20, 'Enable', 'off');
set(handles.radiobutton__worst_40, 'Enable', 'off');
set(handles.radiobutton__worst_60, 'Enable', 'off');
set(handles.radiobutton__worst_80, 'Enable', 'off');
set(handles.radiobutton__worst_100, 'Enable', 'off');
%% % Grades
% %%
grades = [.0 .2 .4 .6 .8 1.];
handles.grades = grades;
worst_grade = 0;
handles.worst_grade = worst_grade;
best_grade = 0;
handles.best_grade = best_grade;
% %% Disabeling "best" radio buttons before a point is selected
% %%
set(handles.radiobutton__best_trajectory__blue, 'Enable', 'off');
set(handles.radiobutton__best_trajectory__purple, 'Enable', 'off');
set(handles.radiobutton__best_trajectory__orange, 'Enable', 'off');
% %% Setting "best" radio button's values to loical false
% %%
set(handles.radiobutton__best_trajectory__blue, 'Value', 0.0);
set(handles.radiobutton__best_trajectory__purple, 'Value', 0.0);
set(handles.radiobutton__best_trajectory__orange, 'Value', 0.0);
% %% Disabeling "worst" radio buttons before a point is selected
% %%
set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'off');
set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'off');
set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'off');
% %% Setting "worst" radio button's values to loical false
% %%
set(handles.radiobutton__worst_trajectory__blue, 'Value', 0.0);
set(handles.radiobutton__worst_trajectory__purple, 'Value', 0.0);
set(handles.radiobutton__worst_trajectory__orange, 'Value', 0.0);
% %% Choose default command line output for gui_tmc_03
% %%
handles.output = hObject;
% %% Update handles structure
% %%
guidata(hObject, handles);

% UIWAIT makes gui_tmc_03 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_tmc_03_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton__grade.
function pushbutton__grade_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__grade (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Enable exporting
% %%
set(handles.pushbutton__export_data, 'Enable', 'on');
%% % Enable points which are not fully grader
%% % First row (see documentation)
% %%
%if handles.labeled_data{1}{1}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{1}{1}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{2}{1}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{2}{1}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{3}{1}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{3}{1}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'on');
%end
%% % Second row (see documentation)
% %%
%if handles.labeled_data{1}{2}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{1}{2}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{2}{2}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{2}{2}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{3}{2}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{3}{2}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'on');
%end
%% % Third row (see documentation)
% %%
%if handles.labeled_data{1}{3}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{1}{3}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{2}{3}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{2}{3}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{3}{3}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{3}{3}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'on');
%end
%% % Fourth row (see documentation)
% %%
%if handles.labeled_data{1}{4}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{1}{4}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{2}{4}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{2}{4}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{3}{4}{1}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'on');
%end
%if handles.labeled_data{3}{4}{2}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'on');
%end
%% % Disable garding
% %%
set(handles.pushbutton__grade, 'Enable', 'off');
%% % Disable loading until new point is selected
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories && handles.switch__collection_labeled == 0
    set(handles.pushbutton__load, 'Enable', 'on');
    set(handles.pushbutton__go_back, 'Enable', 'on');    
elseif handles.iterator__total_labeled ~= handles.number__total_trajectories && handles.switch__collection_labeled == 1
    set(handles.pushbutton__load, 'Enable', 'off');
    set(handles.pushbutton__go_back, 'Enable', 'on');
    handles.switch__collection_labeled = 0;
elseif handles.iterator__total_labeled == handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'off');
    set(handles.pushbutton__go_back, 'Enable', 'off');
end
%% % Logic for recording given grades
% %% First detet what best and wors grade is selected
if get(handles.radiobutton__best_0, 'Value') == 1.0
    handles.best_grade = handles.grades(1);
elseif get(handles.radiobutton__best_20, 'Value') == 1.0
    handles.best_grade = handles.grades(2);
elseif get(handles.radiobutton__best_40, 'Value') == 1.0
    handles.best_grade = handles.grades(3);
elseif get(handles.radiobutton__best_60, 'Value') == 1.0
    handles.best_grade = handles.grades(4);
elseif get(handles.radiobutton__best_80, 'Value') == 1.0
    handles.best_grade = handles.grades(5);
elseif get(handles.radiobutton__best_100, 'Value') == 1.0
    handles.best_grade = handles.grades(6);
end
if get(handles.radiobutton__worst_0, 'Value') == 1.0
    handles.worst_grade = handles.grades(1);
elseif get(handles.radiobutton__worst_20, 'Value') == 1.0
    handles.worst_grade = handles.grades(2);
elseif get(handles.radiobutton__worst_40, 'Value') == 1.0
    handles.worst_grade = handles.grades(3);
elseif get(handles.radiobutton__worst_60, 'Value') == 1.0
    handles.worst_grade = handles.grades(4);
elseif get(handles.radiobutton__worst_80, 'Value') == 1.0
    handles.worst_grade = handles.grades(5);
elseif get(handles.radiobutton__worst_100, 'Value') == 1.0
    handles.worst_grade = handles.grades(6);
end
% %% Check if orange is selected as best
% %%
if get(handles.radiobutton__best_trajectory__orange, 'Value') == 1.0
    % %% Record the corresponding grade
    % %%
    handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2})...
        = handles.best_grade;
    % %% Check if blue is worst
    % %%
    if get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0
        % %% Record the corresponding grade
        % %%
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 2)...
        = handles.worst_grade;
        % %% Compute the remaining grade as the avarage of the given two grades
        % %%
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 1)...
        = handles.best_grade/2 + handles.worst_grade/2;
    % %% Check if purple is worst
    % %%
    elseif get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0
        % %% Record the corresponding grade
        % %% 
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 1)...
        = handles.worst_grade;
        % %% Compute the remaining grade as the avarage of the given two grades
        % %%
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 2)...
        = handles.best_grade/2 + handles.worst_grade/2;
    end
% %% Check if purple is selected as best
% %%
elseif get(handles.radiobutton__best_trajectory__purple, 'Value') == 1.0
    % %% Record the corresponding grade
    % %% 
    handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 1)...
        = handles.best_grade;
    % %% Check if blue is worst
    % %%
    if get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0
        % %% Record the corresponding grade
        % %% 
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 2)...
        = handles.worst_grade;
        % %% Compute the remaining grade as the avarage of the given two grades
        % %%
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2})...
        = handles.best_grade/2 + handles.worst_grade/2;
    % %% Check if orange is worst
    % %%
    elseif get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0
        % %% Record the corresponding grade
        % %% 
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2})...
        = handles.worst_grade;
        % %% Compute the remaining grade as the avarage of the given two grades
        % %%
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 2)...
        = handles.best_grade/2 + handles.worst_grade/2;
    end
% %% Check if blue is selected as best
% %%
elseif get(handles.radiobutton__best_trajectory__blue, 'Value') == 1.0
    % %% Record the corresponding grade
    % %% 
    handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 2)...
        = handles.best_grade;
    % %% Check if purple is worst
    % %%
    if get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0
        % %% Record the corresponding grade
        % %% 
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 1)...
        = handles.worst_grade;
        % %% Compute the remaining grade as the avarage of the given two grades
        % %%
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2})...
        = handles.best_grade/2 + handles.worst_grade/2;
    % %% Check if orange is worst
    % %%
    elseif get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0
        % %% Record the corresponding grade
        % %% 
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2})...
        = handles.worst_grade;
        % %% Compute the remaining grade as the avarage of the given two grades
        % %%
        handles.labeled_data{handles.iterator__engine_speed_points}...
        {handles.iterator__fuel_rate_final_points}...
        {handles.indicator__fuel_rate_initial_points}...
        {1}...
        (handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 1)...
        = handles.best_grade/2 + handles.worst_grade/2;
    end
end
%% % Recording the "iterator total labeled" value
% %%
handles.labeled_data{4} = handles.iterator__total_labeled;
%% % Setting radio buttons for next point
% %% Disabling grading radio buttons
% %%
set(handles.radiobutton__best_0, 'Enable', 'off');
set(handles.radiobutton__best_20, 'Enable', 'off');
set(handles.radiobutton__best_40, 'Enable', 'off');
set(handles.radiobutton__best_60, 'Enable', 'off');
set(handles.radiobutton__best_80, 'Enable', 'off');
set(handles.radiobutton__best_100, 'Enable', 'off');
set(handles.radiobutton__worst_0, 'Enable', 'off');
set(handles.radiobutton__worst_20, 'Enable', 'off');
set(handles.radiobutton__worst_40, 'Enable', 'off');
set(handles.radiobutton__worst_60, 'Enable', 'off');
set(handles.radiobutton__worst_80, 'Enable', 'off');
set(handles.radiobutton__worst_100, 'Enable', 'off');
% %% Setting grading buttons value to 0 
set(handles.radiobutton__best_0, 'Value', 0.0);
set(handles.radiobutton__best_20, 'Value', 0.0);
set(handles.radiobutton__best_40, 'Value', 0.0);
set(handles.radiobutton__best_60, 'Value', 0.0);
set(handles.radiobutton__best_80, 'Value', 0.0);
set(handles.radiobutton__best_100, 'Value', 0.0);
set(handles.radiobutton__worst_0, 'Value', 0.0);
set(handles.radiobutton__worst_20, 'Value', 0.0);
set(handles.radiobutton__worst_40, 'Value', 0.0);
set(handles.radiobutton__worst_60, 'Value', 0.0);
set(handles.radiobutton__worst_80, 'Value', 0.0);
set(handles.radiobutton__worst_100, 'Value', 0.0);
% %% Setting values for "best" radio buttons to logical false
% %%
set(handles.radiobutton__best_trajectory__blue, 'Value', 0.0);
set(handles.radiobutton__best_trajectory__purple, 'Value', 0.0);
set(handles.radiobutton__best_trajectory__orange, 'Value', 0.0);
% %% Setting values for "worst" radio buttons to logical false
% %%
set(handles.radiobutton__worst_trajectory__blue, 'Value', 0.0);
set(handles.radiobutton__worst_trajectory__purple, 'Value', 0.0);
set(handles.radiobutton__worst_trajectory__orange, 'Value', 0.0);
% %% Disabling "best" radio button
% %%
set(handles.radiobutton__best_trajectory__blue, 'Enable', 'off');
set(handles.radiobutton__best_trajectory__purple, 'Enable', 'off');
set(handles.radiobutton__best_trajectory__orange, 'Enable', 'off');
% %% Disabling "worst" radio button
% %%
set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'off');
set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'off');
set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'off');
% %% Update handles structure
% %%
guidata(hObject, handles);


% --- Executes on button press in pushbutton__go_back.
function pushbutton__go_back_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__go_back (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= 0
    handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} = ...
        handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} - 3;
    %% % Updating counters
    % %%
    if handles.indicator__fuel_rate_initial_points == 1
        Tag_name = sprintf('pushbutton__es%d__fr%d__fri%d__cntr', ...
            handles.engine_speed_points(handles.iterator__engine_speed_points), ...
            handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points), ...
            handles.fuel_rate_initial_points1(handles.iterator_range__fuel_rate_initial_points1));
        Tag_temporary = handles.(Tag_name);
        set(Tag_temporary, 'String', ...
            strcat(num2str(handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2}), ...
            '/', num2str(handles.number__trajecotries)));
    end
    if handles.indicator__fuel_rate_initial_points == 2 && handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2) == 2.500000e+00
            Tag_name = sprintf('pushbutton__es%d__fr%d__fri2_5__cntr', ...
            handles.engine_speed_points(handles.iterator__engine_speed_points), ...
            handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points));
        Tag_temporary = handles.(Tag_name);
        set(Tag_temporary, 'String', ...
            strcat(num2str(handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2}), ...
            '/', num2str(handles.number__trajecotries)));
    end
    if handles.indicator__fuel_rate_initial_points == 2 && handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2) ~= 2.500000e+00
        Tag_name = sprintf('pushbutton__es%d__fr%d__fri%d__cntr', ...
            handles.engine_speed_points(handles.iterator__engine_speed_points), ...
            handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points), ...
            handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2));
        Tag_temporary = handles.(Tag_name);
        set(Tag_temporary, 'String', ...
            strcat(num2str(handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2}), ...
            '/', num2str(handles.number__trajecotries)));
    end
end

if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} == 0
    set(handles.pushbutton__go_back, 'Enable', 'off');
else
    set(handles.pushbutton__go_back, 'Enable', 'on');
end
%% % Enable loading
% %%
set(handles.pushbutton__load, 'Enable', 'on');
% %% Update handles structure
% %%
guidata(hObject, handles);


% --- Executes on button press in pushbutton__load.
function pushbutton__load_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% % Disabling exporting when loading new tajectories
% %%
set(handles.pushbutton__export_data, 'Enable', 'off');
%% % Disabling back when loading new tajectories
% %%
set(handles.pushbutton__go_back, 'Enable', 'off');
%% % Enabling radio buttons 
% %% Best
% %%
set(handles.radiobutton__best_trajectory__blue, 'Enable', 'on');
set(handles.radiobutton__best_trajectory__purple, 'Enable', 'on');
set(handles.radiobutton__best_trajectory__orange, 'Enable', 'on');
% %% Worst
% %%
set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'on');
set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'on');
set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'on');
%% % Disabiling unselected operating points and setting the corresponding indecies when chosen trajectories are loaded
% %%
switch handles.point_selected
    %% % First row (see documentation)
    % %%
    case 'es800__fr80__fri50__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 1;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 1;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 1;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        % set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % First row (see documentation)
    % %%
    case 'es800__fr80__fri40__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 1;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 1;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 1;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % First row (see documentation)
    % %%
    case 'es1600__fr80__fri50__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 2;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 1;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 1;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % First row (see documentation)
    % %%
    case 'es1600__fr80__fri40__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 2;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 1;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 1;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % First row (see documentation)
    % %%
    case 'es2400__fr80__fri50__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 3;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 1;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 1;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % First row (see documentation)
    % %%
    case 'es2400__fr80__fri40__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 3;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 1;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 1;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        % set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off'); % uncommented because it's selected
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Second row (see documentation)
    % %%
    case 'es800__fr55__fri80__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 1;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 2;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 2;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        % set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Second row (see documentation)
    % %%
    case 'es800__fr55__fri15__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 1;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 2;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 2;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Second row (see documentation)
    % %%
    case 'es1600__fr55__fri80__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 2;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 2;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 2;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Second row (see documentation)
    % %%
    case 'es1600__fr55__fri15__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 2;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 2;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 2;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Second row (see documentation)
    % %%
    case 'es2400__fr55__fri80__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 3;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 2;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 2;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Second row (see documentation)
    % %%
    case 'es2400__fr55__fri15__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 3;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 2;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 2;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off'); % uncommented because it's selected
        %% % Third row (see documentation)
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Third row (see documentation)
    % %%
    case 'es800__fr30__fri70__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 1;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 3;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 3;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        % set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Third row (see documentation)
    % %%
    case 'es800__fr30__fri2_5__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 1;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 3;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 3;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Third row (see documentation)
    % %%
    case 'es1600__fr30__fri70__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 2;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 3;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 3;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Third row (see documentation)
    % %%
    case 'es1600__fr30__fri2_5__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 2;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 3;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 3;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Third row (see documentation)
    % %%
    case 'es2400__fr30__fri70__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 3;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 3;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 3;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Third row (see documentation)
    % %%
    case 'es2400__fr30__fri2_5__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 3;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 3;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 3;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off'); % uncommented because it's selected
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Fourth row (see documentation)
    % %%
    case 'es800__fr5__fri35__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 1;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 4;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 4;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        % set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Fourth row (see documentation)
    % %%
    case 'es800__fr5__fri45__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 1;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 4;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 4;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Fourth row (see documentation)
    % %%
    case 'es1600__fr5__fri35__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 2;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 4;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 4;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Fourth row (see documentation)
    % %%
    case 'es1600__fr5__fri45__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 2;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 4;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 4;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Fourth row (see documentation)
    % %%
    case 'es2400__fr5__fri35__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 3;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 4;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 1;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points1 = 4;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off'); % uncommented because it's selected
        set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off');
    %% % Fourth row (see documentation)
    % %%
    case 'es2400__fr5__fri45__cntr'
        % %% Which engine speed point
        % %%
        handles.iterator__engine_speed_points = 3;
        % %% Which fuel rate final point
        % %%
        handles.iterator__fuel_rate_final_points = 4;
        % %% Which fuel rate inital ponit 
        % %%
        handles.indicator__fuel_rate_initial_points = 2;
        % %% Which fuel rate inital ponit exactly
        % %%
        handles.iterator_range__fuel_rate_initial_points2 = 4;
        %% % Disabling non-selected points
        %% % First row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri50__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr80__fri40__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr80__fri50__cntr, 'Enable', 'off'); 
        set(handles.pushbutton__es2400__fr80__fri40__cntr, 'Enable', 'off');
        %% % Second row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr55__fri15__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri80__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr55__fri15__cntr, 'Enable', 'off');
        %% % Third row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri70__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'Enable', 'off');
        %% % Fourth row (see documentation)
        % %%
        set(handles.pushbutton__es800__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es800__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri35__cntr, 'Enable', 'off');
        set(handles.pushbutton__es1600__fr5__fri45__cntr, 'Enable', 'off');
        set(handles.pushbutton__es2400__fr5__fri35__cntr, 'Enable', 'off');
        % set(handles.pushbutton__es2400__fr5__fri45__cntr, 'Enable', 'off'); % uncommented because it's selected
end
%% % Disable loading
% %%
set(handles.pushbutton__load, 'Enable', 'off');
%% % Plotting trajectories
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} <= handles.number__trajecotries
    % %% Declare a temporary trajecotry iterator
    % %%
    iterator__temp = handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2};
    %% % Pick upper axes -- boost pressure
    % %%
    axes(handles.axes__boost_pressure);
    % %% Plot Y_u for (picked engine speed point, picked fuel rate final point, picked fuel rate initial point, temporary iterator + 1)
    % %%
    plot(handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 1}{1}{1}, ...
        handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 1}{1}{2}, ...
          '-.+', 'color', [0.36 0.68 0.89], 'LineWidth', 1);
    hold on;
    grid on;
    % %% Plot Y_u for (picked engine speed point, picked fuel rate final point, picked fuel rate initial point, temporary iterator + 2)
    % %%
    plot(handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 2}{1}{1}, ...
        handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 2}{1}{2}, ...
        '-.o', 'color', [0.65 0.41 0.74], 'LineWidth', 1);
    hold on;
    % %% Plot Y_u for (picked engine speed point, picked fuel rate final point, picked fuel rate initial point, temporary iterator + 3)
    % %%
    plot(handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 3}{1}{1}, ...
        handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 3}{1}{2}, ...
        '-.d', 'color', [0.96 0.69 0.25], 'LineWidth', 1);
    hold on
    % %% Plot Y_ss for (picked engine speed point, picked fuel rate final point, picked fuel rate initial point, temporary iterator + 1)
    % %%
    plot(handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 1}{1}{1}, ...
        handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 1}{1}{3}, ...
        'color', 'red', 'LineWidth', 1.5, 'LineStyle', '--');
    set(get(handles.axes__boost_pressure, 'YLabel'), 'String', 'Boost pressure (kPa)');
    hold off;
    %% % Pick lower axes -- egr rate
    % %%
    axes(handles.axes__egr_rate);
    % %% Plot Y_u for (picked engine speed point, picked fuel rate final point, picked fuel rate initial point, temporary iterator + 1)
    % %%
    plot(handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 1}{2}{1}, ...
        handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 1}{2}{2}, ...
        '-.+', 'color', [0.36 0.68 0.89], 'LineWidth', 1);
    hold on;
    grid on
    % %% Plot Y_u for (picked engine speed point, picked fuel rate final point, picked fuel rate initial point, temporary iterator + 2)
    % %%
    plot(handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 2}{2}{1}, ...
        handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 2}{2}{2}, ...
        '-.o', 'color', [0.65 0.41 0.74], 'LineWidth', 1);
    hold on;
    % %% Plot Y_u for (picked engine speed point, picked fuel rate final point, picked fuel rate initial point, temporary iterator + 3)
    % %%
    plot(handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 3}{2}{1}, ...
        handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 3}{2}{2}, ...
        '-.d', 'color', [0.96 0.69 0.25], 'LineWidth', 1);
    hold on
    % %% Plot Y_ss for (picked engine speed point, picked fuel rate final point, picked fuel rate initial point, temporary iterator + 1)
    % %%
    plot(handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 1}{2}{1}, ...
        handles.trajectory_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{iterator__temp + 1}{2}{3}, ...
        'color', 'red', 'LineWidth', 1.5, 'LineStyle', '--'); 
    set(get(handles.axes__egr_rate, 'YLabel'), 'String', 'EGR rate (%)');
    hold off;
    %% % Updating the corresponding iterator
    % %%
    handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} = ...
        handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} + 3;
    % %% Updating the total number of graded (labeled) trajectories
    % %%
    handles.iterator__total_labeled = handles.iterator__total_labeled + 3;
    %% % Updating counters
    % %%
    if handles.indicator__fuel_rate_initial_points == 1
        Tag_name = sprintf('pushbutton__es%d__fr%d__fri%d__cntr', ...
            handles.engine_speed_points(handles.iterator__engine_speed_points), ...
            handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points), ...
            handles.fuel_rate_initial_points1(handles.iterator_range__fuel_rate_initial_points1));
        Tag_temporary = handles.(Tag_name);
        set(Tag_temporary, 'String', ...
            strcat(num2str(handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2}), ...
            '/', num2str(handles.number__trajecotries)));
    end
    if handles.indicator__fuel_rate_initial_points == 2 && handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2) == 2.500000e+00
            Tag_name = sprintf('pushbutton__es%d__fr%d__fri2_5__cntr', ...
            handles.engine_speed_points(handles.iterator__engine_speed_points), ...
            handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points));
        Tag_temporary = handles.(Tag_name);
        set(Tag_temporary, 'String', ...
            strcat(num2str(handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2}), ...
            '/', num2str(handles.number__trajecotries)));
    end
    if handles.indicator__fuel_rate_initial_points == 2 && handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2) ~= 2.500000e+00
        Tag_name = sprintf('pushbutton__es%d__fr%d__fri%d__cntr', ...
            handles.engine_speed_points(handles.iterator__engine_speed_points), ...
            handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points), ...
            handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2));
        Tag_temporary = handles.(Tag_name);
        set(Tag_temporary, 'String', ...
            strcat(num2str(handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2}), ...
            '/', num2str(handles.number__trajecotries)));
    end
    %% % Disabling the point for which all trajectories are graded
    % %%
    if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} == handles.number__trajecotries
%         if handles.indicator__fuel_rate_initial_points == 1
%             Tag_name = sprintf('pushbutton__es%d__fr%d__fri%d__cntr', ...
%                 handles.engine_speed_points(handles.iterator__engine_speed_points), ...
%                 handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points), ...
%                 handles.fuel_rate_initial_points1(handles.iterator_range__fuel_rate_initial_points1));
%             Tag_temporary = handles.(Tag_name);
%             set(Tag_temporary, 'Enable', 'off');
%         end
%         if handles.indicator__fuel_rate_initial_points == 2 && handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2) == 2.500000e+00
%                 Tag_name = sprintf('pushbutton__es%d__fr%d__fri2_5__cntr', ...
%                 handles.engine_speed_points(handles.iterator__engine_speed_points), ...
%                 handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points));
%             Tag_temporary = handles.(Tag_name);
%             set(Tag_temporary, 'Enable', 'off');
%         end
%         if handles.indicator__fuel_rate_initial_points == 2 && handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2) ~= 2.500000e+00
%             Tag_name = sprintf('pushbutton__es%d__fr%d__fri%d__cntr', ...
%                 handles.engine_speed_points(handles.iterator__engine_speed_points), ...
%                 handles.fuel_rate_final_points(handles.iterator__fuel_rate_final_points), ...
%                 handles.fuel_rate_initial_points2(handles.iterator_range__fuel_rate_initial_points2));
%             Tag_temporary = handles.(Tag_name);
%             set(Tag_temporary, 'Enable', 'off');
%         end
        handles.switch__collection_labeled = 1;
    end
end
guidata(hObject, handles);


% --- Executes on button press in pushbutton__export_data.
function pushbutton__export_data_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__export_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% % Exporting data
labeled_data = handles.labeled_data;
save labeled_data.mat labeled_data
guidata(hObject, handles);


% --- Executes on slider movement.
function slider__worst_trajectory_Callback(hObject, eventdata, handles)
% hObject    handle to slider__worst_trajectory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider__worst_trajectory_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider__worst_trajectory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider__best_trajectory_Callback(hObject, eventdata, handles)
% hObject    handle to slider__best_trajectory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider__best_trajectory_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider__best_trajectory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in radiobutton__best_trajectory__orange.
function radiobutton__best_trajectory__orange_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_trajectory__orange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Logic for enabling and unenabling radio buttons
% %%
if get(handles.radiobutton__best_trajectory__orange, 'Value') == 1.0
    set(handles.radiobutton__best_0, 'Enable', 'on');
    set(handles.radiobutton__best_20, 'Enable', 'on');
    set(handles.radiobutton__best_40, 'Enable', 'on');
    set(handles.radiobutton__best_60, 'Enable', 'on');
    set(handles.radiobutton__best_80, 'Enable', 'on');
    set(handles.radiobutton__best_100, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'off');
    set(handles.radiobutton__best_trajectory__purple, 'Enable', 'off');
    set(handles.radiobutton__best_trajectory__blue, 'Enable', 'off');
else
    set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__purple, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__blue, 'Enable', 'on');
    set(handles.radiobutton__best_0, 'Enable', 'off');
    set(handles.radiobutton__best_20, 'Enable', 'off');
    set(handles.radiobutton__best_40, 'Enable', 'off');
    set(handles.radiobutton__best_60, 'Enable', 'off');
    set(handles.radiobutton__best_80, 'Enable', 'off');
    set(handles.radiobutton__best_100, 'Enable', 'off');
end
%% % Logic for enabling and unenabling graing
% %%
if (get(handles.radiobutton__best_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__orange, 'Value') == 1.0) ...
        && (get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0)
    set(handles.pushbutton__grade, 'Enable', 'on');
else
    set(handles.pushbutton__grade, 'Enable', 'off');
end
% Update handles structure
guidata(hObject, handles);

% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_trajectory__orange


% --- Executes on button press in radiobutton__best_trajectory__purple.
function radiobutton__best_trajectory__purple_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_trajectory__purple (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Logic for enabling and unenabling radio buttons
% %%
if get(handles.radiobutton__best_trajectory__purple, 'Value') == 1.0
    set(handles.radiobutton__best_0, 'Enable', 'on');
    set(handles.radiobutton__best_20, 'Enable', 'on');
    set(handles.radiobutton__best_40, 'Enable', 'on');
    set(handles.radiobutton__best_60, 'Enable', 'on');
    set(handles.radiobutton__best_80, 'Enable', 'on');
    set(handles.radiobutton__best_100, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'off');
    set(handles.radiobutton__best_trajectory__blue, 'Enable', 'off');
    set(handles.radiobutton__best_trajectory__orange, 'Enable', 'off');
else
    set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__blue, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__orange, 'Enable', 'on');
    set(handles.radiobutton__best_0, 'Enable', 'off');
    set(handles.radiobutton__best_20, 'Enable', 'off');
    set(handles.radiobutton__best_40, 'Enable', 'off');
    set(handles.radiobutton__best_60, 'Enable', 'off');
    set(handles.radiobutton__best_80, 'Enable', 'off');
    set(handles.radiobutton__best_100, 'Enable', 'off');
end
%% % Logic for enabling and unenabling graing
% %%
if (get(handles.radiobutton__best_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__orange, 'Value') == 1.0) ...
        && (get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0)
    set(handles.pushbutton__grade, 'Enable', 'on');
else
    set(handles.pushbutton__grade, 'Enable', 'off');
end

% Update handles structure
guidata(hObject, handles);

% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_trajectory__purple


% --- Executes on button press in radiobutton__best_trajectory__blue.
function radiobutton__best_trajectory__blue_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_trajectory__blue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Logic for enabling and unenabling radio buttons
% %%
if get(handles.radiobutton__best_trajectory__blue, 'Value') == 1.0
    set(handles.radiobutton__best_0, 'Enable', 'on');
    set(handles.radiobutton__best_20, 'Enable', 'on');
    set(handles.radiobutton__best_40, 'Enable', 'on');
    set(handles.radiobutton__best_60, 'Enable', 'on');
    set(handles.radiobutton__best_80, 'Enable', 'on');
    set(handles.radiobutton__best_100, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'off');
    set(handles.radiobutton__best_trajectory__orange, 'Enable', 'off');
    set(handles.radiobutton__best_trajectory__purple, 'Enable', 'off');
else
    set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__orange, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__purple, 'Enable', 'on');
    set(handles.radiobutton__best_0, 'Enable', 'off');
    set(handles.radiobutton__best_20, 'Enable', 'off');
    set(handles.radiobutton__best_40, 'Enable', 'off');
    set(handles.radiobutton__best_60, 'Enable', 'off');
    set(handles.radiobutton__best_80, 'Enable', 'off');
    set(handles.radiobutton__best_100, 'Enable', 'off');
end
%% % Logic for enabling and unenabling graing
% %%
if (get(handles.radiobutton__best_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__orange, 'Value') == 1.0) ...
        && (get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0)
    set(handles.pushbutton__grade, 'Enable', 'on');
else
    set(handles.pushbutton__grade, 'Enable', 'off');
end
% Update handles structure
guidata(hObject, handles);

% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_trajectory__blue


% --- Executes on button press in radiobutton__worst_trajectory__orange.
function radiobutton__worst_trajectory__orange_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_trajectory__orange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Logic for enabling and unenabling radio buttons
% %%
if get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0
    set(handles.radiobutton__worst_0, 'Enable', 'on');
    set(handles.radiobutton__worst_20, 'Enable', 'on');
    set(handles.radiobutton__worst_40, 'Enable', 'on');
    set(handles.radiobutton__worst_60, 'Enable', 'on');
    set(handles.radiobutton__worst_80, 'Enable', 'on');
    set(handles.radiobutton__worst_100, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__orange, 'Enable', 'off');
    set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'off');
    set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'off');
else
    set(handles.radiobutton__best_trajectory__orange, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'on');
    set(handles.radiobutton__worst_0, 'Enable', 'off');
    set(handles.radiobutton__worst_20, 'Enable', 'off');
    set(handles.radiobutton__worst_40, 'Enable', 'off');
    set(handles.radiobutton__worst_60, 'Enable', 'off');
    set(handles.radiobutton__worst_80, 'Enable', 'off');
    set(handles.radiobutton__worst_100, 'Enable', 'off');
end
%% % Logic for enabling and unenabling graing
% %%
if (get(handles.radiobutton__best_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__orange, 'Value') == 1.0) ...
        && (get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0)
    set(handles.pushbutton__grade, 'Enable', 'on');
else
    set(handles.pushbutton__grade, 'Enable', 'off');
end
% Update handles structure
guidata(hObject, handles);

% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_trajectory__orange


% --- Executes on button press in radiobutton__worst_trajectory__purple.
function radiobutton__worst_trajectory__purple_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_trajectory__purple (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Logic for enabling and unenabling radio buttons
% %%
if get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0
    set(handles.radiobutton__worst_0, 'Enable', 'on');
    set(handles.radiobutton__worst_20, 'Enable', 'on');
    set(handles.radiobutton__worst_40, 'Enable', 'on');
    set(handles.radiobutton__worst_60, 'Enable', 'on');
    set(handles.radiobutton__worst_80, 'Enable', 'on');
    set(handles.radiobutton__worst_100, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__purple, 'Enable', 'off');
    set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'off');
    set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'off');
else
    set(handles.radiobutton__best_trajectory__purple, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__blue, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'on');
    set(handles.radiobutton__worst_0, 'Enable', 'off');
    set(handles.radiobutton__worst_20, 'Enable', 'off');
    set(handles.radiobutton__worst_40, 'Enable', 'off');
    set(handles.radiobutton__worst_60, 'Enable', 'off');
    set(handles.radiobutton__worst_80, 'Enable', 'off');
    set(handles.radiobutton__worst_100, 'Enable', 'off');
end
%% % Logic for enabling and unenabling graing
% %%
if (get(handles.radiobutton__best_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__orange, 'Value') == 1.0) ...
        && (get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0)
    set(handles.pushbutton__grade, 'Enable', 'on');
else
    set(handles.pushbutton__grade, 'Enable', 'off');
end
% Update handles structure
guidata(hObject, handles);

% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_trajectory__purple


% --- Executes on button press in radiobutton__worst_trajectory__blue.
function radiobutton__worst_trajectory__blue_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_trajectory__blue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Logic for enabling and unenabling radio buttons
% %%
if get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0
    set(handles.radiobutton__worst_0, 'Enable', 'on');
    set(handles.radiobutton__worst_20, 'Enable', 'on');
    set(handles.radiobutton__worst_40, 'Enable', 'on');
    set(handles.radiobutton__worst_60, 'Enable', 'on');
    set(handles.radiobutton__worst_80, 'Enable', 'on');
    set(handles.radiobutton__worst_100, 'Enable', 'on');
    set(handles.radiobutton__best_trajectory__blue, 'Enable', 'off');
    set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'off');
    set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'off');
else
    set(handles.radiobutton__best_trajectory__blue, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__orange, 'Enable', 'on');
    set(handles.radiobutton__worst_trajectory__purple, 'Enable', 'on');
    set(handles.radiobutton__worst_0, 'Enable', 'off');
    set(handles.radiobutton__worst_20, 'Enable', 'off');
    set(handles.radiobutton__worst_40, 'Enable', 'off');
    set(handles.radiobutton__worst_60, 'Enable', 'off');
    set(handles.radiobutton__worst_80, 'Enable', 'off');
    set(handles.radiobutton__worst_100, 'Enable', 'off');
end
%% % Logic for enabling and unenabling graing
% %%
if (get(handles.radiobutton__best_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__best_trajectory__orange, 'Value') == 1.0) ...
        && (get(handles.radiobutton__worst_trajectory__blue, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__purple, 'Value') == 1.0 ||...
        get(handles.radiobutton__worst_trajectory__orange, 'Value') == 1.0)
    set(handles.pushbutton__grade, 'Enable', 'on');
else
    set(handles.pushbutton__grade, 'Enable', 'off');
end
% %% Update handles structure
% %%
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_trajectory__blue

% --- Executes on button press in pushbutton__es800__fr80__fri50__cntr.
function pushbutton__es800__fr80__fri50__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es800__fr80__fri50__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 1;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 1;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es800__fr80__fri50__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es800__fr80__fri40__cntr.
function pushbutton__es800__fr80__fri40__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es800__fr80__fri40__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 1;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 1;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es800__fr80__fri40__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es1600__fr80__fri40__cntr.
function pushbutton__es1600__fr80__fri40__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es1600__fr80__fri40__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 2;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 1;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es1600__fr80__fri40__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es1600__fr80__fri50__cntr.
function pushbutton__es1600__fr80__fri50__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es1600__fr80__fri50__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 2;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 1;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es1600__fr80__fri50__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es2400__fr80__fri40__cntr.
function pushbutton__es2400__fr80__fri40__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es2400__fr80__fri40__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 3;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 1;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 1]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es2400__fr80__fri40__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es2400__fr80__fri50__cntr.
function pushbutton__es2400__fr80__fri50__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es2400__fr80__fri50__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 3;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 1;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es2400__fr80__fri50__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es800__fr55__fri15__cntr.
function pushbutton__es800__fr55__fri15__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es800__fr55__fri15__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 1;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 2;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es800__fr55__fri15__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es800__fr55__fri80__cntr.
function pushbutton__es800__fr55__fri80__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es800__fr55__fri80__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 1;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 2;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es800__fr55__fri80__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es1600__fr55__fri15__cntr.
function pushbutton__es1600__fr55__fri15__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es1600__fr55__fri15__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 2;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 2;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es1600__fr55__fri15__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es1600__fr55__fri80__cntr.
function pushbutton__es1600__fr55__fri80__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es1600__fr55__fri80__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 2;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 2;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es1600__fr55__fri80__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es2400__fr55__fri15__cntr.
function pushbutton__es2400__fr55__fri15__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es2400__fr55__fri15__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 3;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 2;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 1]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es2400__fr55__fri15__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es2400__fr55__fri80__cntr.
function pushbutton__es2400__fr55__fri80__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es2400__fr55__fri80__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 3;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 2;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es2400__fr55__fri80__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es800__fr30__fri2_5__cntr.
function pushbutton__es800__fr30__fri2_5__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es800__fr30__fri2_5__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 1;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 3;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es800__fr30__fri2_5__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es800__fr30__fri70__cntr.
function pushbutton__es800__fr30__fri70__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es800__fr30__fri70__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 1;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 3;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es800__fr30__fri70__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es1600__fr30__fri2_5__cntr.
function pushbutton__es1600__fr30__fri2_5__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es1600__fr30__fri2_5__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 2;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 3;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es1600__fr30__fri2_5__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es1600__fr30__fri70__cntr.
function pushbutton__es1600__fr30__fri70__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es1600__fr30__fri70__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 2;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 3;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es1600__fr30__fri70__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es2400__fr30__fri2_5__cntr.
function pushbutton__es2400__fr30__fri2_5__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es2400__fr30__fri2_5__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 3;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 3;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 1]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es2400__fr30__fri2_5__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es2400__fr30__fri70__cntr.
function pushbutton__es2400__fr30__fri70__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es2400__fr30__fri70__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 3;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 3;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es2400__fr30__fri70__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es800__fr5__fri45__cntr.
function pushbutton__es800__fr5__fri45__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es800__fr5__fri45__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 1;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 4;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es800__fr5__fri45__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es800__fr5__fri35__cntr.
function pushbutton__es800__fr5__fri35__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es800__fr5__fri35__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 1;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 4;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es800__fr5__fri35__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es1600__fr5__fri45__cntr.
function pushbutton__es1600__fr5__fri45__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es1600__fr5__fri45__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 2;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 4;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es1600__fr5__fri45__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es1600__fr5__fri35__cntr.
function pushbutton__es1600__fr5__fri35__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es1600__fr5__fri35__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 2;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 4;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es1600__fr5__fri35__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es2400__fr5__fri45__cntr.
function pushbutton__es2400__fr5__fri45__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es2400__fr5__fri45__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 3;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 4;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 2;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 1]);
%% % Setting point selected
% %%
handles.point_selected = 'es2400__fr5__fri45__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);

% --- Executes on button press in pushbutton__es2400__fr5__fri35__cntr.
function pushbutton__es2400__fr5__fri35__cntr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton__es2400__fr5__fri35__cntr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% % Setting the data
% %% Which engine speed point
% %%
handles.iterator__engine_speed_points = 3;
% %% Which fuel rate final point
% %%
handles.iterator__fuel_rate_final_points = 4;
% %% Which fuel rate inital ponit 
% %%
handles.indicator__fuel_rate_initial_points = 1;
%% % Indicating selected point with blue color
% %%
%% % First row (see documentation)
% %%
set(handles.pushbutton__es800__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri50__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr80__fri40__cntr, 'ForegroundColor', [0 0 0]);
%% % Second row (see documentation)
% %%
set(handles.pushbutton__es800__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri80__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr55__fri15__cntr, 'ForegroundColor', [0 0 0]);
%% % Third row (see documentation)
% %%
set(handles.pushbutton__es800__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri70__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr30__fri2_5__cntr, 'ForegroundColor', [0 0 0]);
%% % Fourth row (see documentation)
% %%
set(handles.pushbutton__es800__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es800__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri35__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es1600__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
set(handles.pushbutton__es2400__fr5__fri35__cntr, 'ForegroundColor', [0 0 1]);
set(handles.pushbutton__es2400__fr5__fri45__cntr, 'ForegroundColor', [0 0 0]);
%% % Setting point selected
% %%
handles.point_selected = 'es2400__fr5__fri35__cntr';
%% % Disable "Load" button if all trajectories have been graded
% %%
if handles.iterator__total_labeled ~= handles.number__total_trajectories
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Disable load if its trajectoreis have been labeled
% %%
if handles.labeled_data{handles.iterator__engine_speed_points}{handles.iterator__fuel_rate_final_points}{handles.indicator__fuel_rate_initial_points}{2} ~= handles.number__trajecotries
    set(handles.pushbutton__load, 'Enable', 'on');
else
    set(handles.pushbutton__load, 'Enable', 'off');
end
%% % Enable go back button
% %%
set(handles.pushbutton__go_back, 'Enable', 'on');
% %%
guidata(hObject, handles);


% --- Executes on button press in radiobutton__worst_0.
function radiobutton__worst_0_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__worst_20, 'Value', 0.0);
set(handles.radiobutton__worst_40, 'Value', 0.0);
set(handles.radiobutton__worst_60, 'Value', 0.0);
set(handles.radiobutton__worst_80, 'Value', 0.0);
set(handles.radiobutton__worst_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_0
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__worst_20.
function radiobutton__worst_20_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__worst_0, 'Value', 0.0);
set(handles.radiobutton__worst_40, 'Value', 0.0);
set(handles.radiobutton__worst_60, 'Value', 0.0);
set(handles.radiobutton__worst_80, 'Value', 0.0);
set(handles.radiobutton__worst_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_20
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__worst_40.
function radiobutton__worst_40_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__worst_0, 'Value', 0.0);
set(handles.radiobutton__worst_20, 'Value', 0.0);
set(handles.radiobutton__worst_60, 'Value', 0.0);
set(handles.radiobutton__worst_80, 'Value', 0.0);
set(handles.radiobutton__worst_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_40
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__worst_60.
function radiobutton__worst_60_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_60 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__worst_0, 'Value', 0.0);
set(handles.radiobutton__worst_20, 'Value', 0.0);
set(handles.radiobutton__worst_40, 'Value', 0.0);
set(handles.radiobutton__worst_80, 'Value', 0.0);
set(handles.radiobutton__worst_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_60
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__worst_80.
function radiobutton__worst_80_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_80 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__worst_0, 'Value', 0.0);
set(handles.radiobutton__worst_20, 'Value', 0.0);
set(handles.radiobutton__worst_40, 'Value', 0.0);
set(handles.radiobutton__worst_60, 'Value', 0.0);
set(handles.radiobutton__worst_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_80
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__worst_100.
function radiobutton__worst_100_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__worst_100 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__worst_0, 'Value', 0.0);
set(handles.radiobutton__worst_20, 'Value', 0.0);
set(handles.radiobutton__worst_40, 'Value', 0.0);
set(handles.radiobutton__worst_60, 'Value', 0.0);
set(handles.radiobutton__worst_80, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__worst_100
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__best_0.
function radiobutton__best_0_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__best_20, 'Value', 0.0);
set(handles.radiobutton__best_40, 'Value', 0.0);
set(handles.radiobutton__best_60, 'Value', 0.0);
set(handles.radiobutton__best_80, 'Value', 0.0);
set(handles.radiobutton__best_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_0
% %%
guidata(hObject, handles);


% --- Executes on button press in radiobutton__best_20.
function radiobutton__best_20_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__best_0, 'Value', 0.0);
set(handles.radiobutton__best_40, 'Value', 0.0);
set(handles.radiobutton__best_60, 'Value', 0.0);
set(handles.radiobutton__best_80, 'Value', 0.0);
set(handles.radiobutton__best_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_20
% %%
guidata(hObject, handles);


% --- Executes on button press in radiobutton__best_40.
function radiobutton__best_40_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__best_0, 'Value', 0.0);
set(handles.radiobutton__best_20, 'Value', 0.0);
set(handles.radiobutton__best_60, 'Value', 0.0);
set(handles.radiobutton__best_80, 'Value', 0.0);
set(handles.radiobutton__best_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_40
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__best_60.
function radiobutton__best_60_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_60 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__best_0, 'Value', 0.0);
set(handles.radiobutton__best_20, 'Value', 0.0);
set(handles.radiobutton__best_40, 'Value', 0.0);
set(handles.radiobutton__best_80, 'Value', 0.0);
set(handles.radiobutton__best_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_60
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__best_80.
function radiobutton__best_80_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_80 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__best_0, 'Value', 0.0);
set(handles.radiobutton__best_20, 'Value', 0.0);
set(handles.radiobutton__best_40, 'Value', 0.0);
set(handles.radiobutton__best_60, 'Value', 0.0);
set(handles.radiobutton__best_100, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_80
% %%
guidata(hObject, handles);

% --- Executes on button press in radiobutton__best_100.
function radiobutton__best_100_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton__best_100 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton__best_0, 'Value', 0.0);
set(handles.radiobutton__best_20, 'Value', 0.0);
set(handles.radiobutton__best_40, 'Value', 0.0);
set(handles.radiobutton__best_60, 'Value', 0.0);
set(handles.radiobutton__best_80, 'Value', 0.0);
% %%
% Hint: get(hObject,'Value') returns toggle state of radiobutton__best_100
% %%
guidata(hObject, handles);