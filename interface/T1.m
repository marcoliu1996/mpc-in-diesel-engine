function varargout = T1(varargin)
% T1 MATLAB code for T1.fig
%      T1, by itself, creates a new T1 or raises the existing
%      singleton*.
%
%      H = T1 returns the handle to a new T1 or the handle to
%      the existing singleton*.
%
%      T1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in T1.M with the given input arguments.
%
%      T1('Property','Value',...) creates a new T1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before T1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to T1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help T1

% Last Modified by GUIDE v2.5 13-Jul-2018 15:23:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @T1_OpeningFcn, ...
                   'gui_OutputFcn',  @T1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before T1 is made visible.
function T1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to T1 (see VARARGIN)

% Choose default command line output for T1
handles.output = hObject;
time = load('time.mat');
handles.time = time.time;
BP = load('BoostPressure.mat');
handles.BP = BP.BP;
BP_ref = load('BoostPressurereference.mat');
handles.BP_ref = BP_ref.BP_ref;
egrrate = load('EGRrate.mat');
handles.egrrate = egrrate.egrrate;
egrrate_ref = load('EGRrateref.mat');
handles.egrrate_ref = egrrate_ref.egrrate_ref;
engineSpeed = load('EngineSpeed.mat');
handles.engineSpeed = engineSpeed.enginespeed;
fuelRate = load('FuelRate.mat');
handles.fuelRate = fuelRate.fuelrate;
handles.endPT = '';
handles.previousPT = '';
handles.select = 0;
handles.currentSelect = '';
handles.currentBPFill = '';
handles.currentBPfullFill = '';
handles.currentEGRFill = '';
handles.currentEGRfullFill = '';
handles.currentParametersFill = '';
handles.highlights = '';
handles.finalTime = handles.time(end);
handles.dataLength = length(handles.time);
handles.parameterLim=[0 800 5 30;0 800 30 55;0 800 55 80;
                            800 1600 5 30;800 1600 30 55;800 1600 55 80;
                            1600 2400 5 30;1600 2400 30 55;1600 2400 55 80];
handles.grade = 100;
handles.database ='';
set(handles.selectedList,'Max',3);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes T1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = T1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure

varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function BP_Time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BP_Time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate BP_Time


% --- Executes on button press in Plot.
function Plot_Callback(hObject, eventdata, handles)
% hObject    handle to Plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% Loading Data
% uigetfile


%% BP Figure
axes(handles.BP_Time);
plot(handles.time,handles.BP,'b')
hold on
plot(handles.time,handles.BP_ref,'r')
xlim([0 handles.finalTime]);
% legend('BP','BP Reference')
guidata(hObject, handles);

%% EGR Rate Figure
axes(handles.EGR_Rate);
plot(handles.time,handles.egrrate,'b')
hold on
xlim([0 handles.finalTime]);
ylim([0 1]);
plot(handles.time,handles.egrrate_ref,'color','r')
guidata(hObject, handles);

%% BPfull Figure
axes(handles.BPfull);
plot(handles.time,handles.BP,'b')
hold on
plot(handles.time,handles.BP_ref,'r')
xlim([0 handles.finalTime]);
% legend('BP','BP Reference')
guidata(hObject, handles);

%% EGRfull Figure
axes(handles.EGRfull);
plot(handles.time,handles.egrrate,'b')
hold on
xlim([0 handles.finalTime]);
ylim([0 1]);
plot(handles.time,handles.egrrate_ref,'color','r')
guidata(hObject, handles);
%% Parameters Figure
axes(handles.parameters);
plot(handles.fuelRate,handles.engineSpeed,'b');
xlim([-60 60]);
ylim([600 2500]);
hold on;
guidata(hObject, handles);

% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)

if hObject.Value == 1
	
    handles.select = 1;
else
	
    handles.select = 0;
end
guidata(hObject, handles);
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
if handles.select == 1
    handles.previousPT = get(hObject,'CurrentPoint');
end


guidata(hObject, handles);
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.

function figure1_WindowButtonUpFcn(hObject, eventdata, handles)
if  handles.select == 1
    handles.endPT = get(hObject,'CurrentPoint');
    positionEGR = get(handles.EGR_Rate,'position');
    dataLength = round((handles.endPT(1)-handles.previousPT(1))/positionEGR(3)*(handles.EGR_Rate.XLim(2)-handles.EGR_Rate.XLim(1))/handles.finalTime*handles.dataLength);
    dataStart = round((handles.previousPT(1)-positionEGR(1))/positionEGR(3)*(handles.EGR_Rate.XLim(2)-handles.EGR_Rate.XLim(1))/handles.finalTime*handles.dataLength+handles.EGR_Rate.XLim(1)/handles.finalTime*handles.dataLength);
    dataEnd = dataLength+dataStart;
    
      
    if dataStart > dataEnd
        temp = dataStart;
        dataStart = dataEnd;
        dataEnd = temp;
    end
    
    handles.start = dataStart;
    handles.end = dataEnd;
  
    
        delete(handles.highlights);
        delete(handles.currentBPFill);
        delete(handles.currentBPfullFill);
        delete(handles.currentEGRFill);
        delete(handles.currentEGRfullFill);
        delete(handles.currentParametersFill);
    
    axes(handles.EGR_Rate);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentEGRFill = fill([handles.time(dataStart) handles.time(dataStart) handles.time(dataEnd) handles.time(dataEnd)],[0 1 1 0],'m','facealpha',.1,'edgecolor','r');
    
    axes(handles.EGRfull);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentEGRfullFill = fill([handles.time(dataStart) handles.time(dataStart) handles.time(dataEnd) handles.time(dataEnd)],[0 1 1 0],'m','facealpha',.1,'edgecolor','r');
    guidata(hObject, handles);
    
    axes(handles.BP_Time);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentBPFill = fill([handles.time(dataStart) handles.time(dataStart) handles.time(dataEnd) handles.time(dataEnd)],[90 190 190 90],'m','facealpha',.1,'edgecolor','r');
    
    axes(handles.BPfull);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentBPfullFill = fill([handles.time(dataStart) handles.time(dataStart) handles.time(dataEnd) handles.time(dataEnd)],[90 190 190 90],'m','facealpha',.1,'edgecolor','r');
    guidata(hObject, handles);
    
    
    axes(handles.parameters);
     for i = 1:length(handles.parameterLim)
                for j = dataStart:dataEnd
                    if(handles.engineSpeed(j)>handles.parameterLim(i,1) && handles.engineSpeed(j)<handles.parameterLim(i,2) && handles.fuelRate(j)>handles.parameterLim(i,3) && handles.fuelRate(j)<handles.parameterLim(i,4))
                        handles.currentParametersFill = [handles.currentParametersFill fill([handles.parameterLim(i,3) handles.parameterLim(i,4) handles.parameterLim(i,4) handles.parameterLim(i,3)],[handles.parameterLim(i,1) handles.parameterLim(i,1) handles.parameterLim(i,2) handles.parameterLim(i,2)],'m','facealpha',.1,'edgecolor','r')];
                        guidata(hObject, handles); 
                        break;
                    end
                end
     end
     guidata(hObject, handles);       
     %plot(handles.engineSpeed(dataStart:dataEnd),handles.fuelRate(dataStart:dataEnd));
   
    
    msgstr = sprintf('x = %3.3f; y = %3.3f',handles.endPT(1),handles.endPT(2)); %????
    msgstr2 = sprintf('x = %3.3f; y = %3.3f',handles.previousPT(1),handles.previousPT(2)); %????
    xianshi= uicontrol('style','text','position',[100 100 100 30],'string',msgstr);
    xianshi2= uicontrol('style','text','position',[100 200 100 30],'string',msgstr2);
    
    if handles.previousPT(1)<positionEGR(1)||handles.previousPT(1)>positionEGR(1)+positionEGR(3)
        delete(handles.currentEGRfullFill);
        delete(handles.currentBPfullFill);
    end
end
guidata(hObject, handles);

% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in grade.
function grade_Callback(hObject, eventdata, handles)
% hObject    handle to grade (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete(handles.highlights);
if handles.select ==1
dataStart = handles.start;
dataEnd = handles.end;
axes(handles.EGR_Rate);
handles.highlights = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
axes(handles.EGRfull);
handles.highlights = [handles.highlights plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y')];
axes(handles.BP_Time);
handles.highlights = [handles.highlights plot(handles.time(dataStart:dataEnd),handles.BP(dataStart:dataEnd),'y')];
axes(handles.BPfull);
handles.highlights = [handles.highlights plot(handles.time(dataStart:dataEnd),handles.BP(dataStart:dataEnd),'y')];
axes(handles.parameters);
handles.highlights = [handles.highlights plot(handles.fuelRate(dataStart:dataEnd),handles.engineSpeed(dataStart:dataEnd),'y')];
guidata(hObject, handles);
end

newRow = num2str([dataStart/handles.dataLength*handles.finalTime dataEnd/handles.dataLength*handles.finalTime handles.grade]);
handles.database = [handles.database;newRow];
guidata(hObject, handles);
set(handles.selectedList,'String',{handles.database});



% --- Executes during object creation, after setting all properties.
function selectedList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectedList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in selectedList.
function selectedList_Callback(hObject, eventdata, handles)
% hObject    handle to selectedList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = cellstr(get(hObject,'String'));
selectIndex = get(hObject,'Value');
delete(handles.highlights);
        delete(handles.currentBPFill);
        delete(handles.currentBPfullFill);
        delete(handles.currentEGRFill);
        delete(handles.currentEGRfullFill);
        delete(handles.currentParametersFill);
for index = selectIndex(1:end)
selected = contents{index};
selected = str2num(selected);
dataStart = round(selected(1)*handles.dataLength/handles.finalTime);
dataEnd = round(selected(2)*handles.dataLength/handles.finalTime);

        
 
     i = 1;  
    axes(handles.EGR_Rate);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentEGRFill = cat(1,handles.currentEGRFill,fill([handles.time(dataStart(i)) handles.time(dataStart(i)) handles.time(dataEnd(i)) handles.time(dataEnd(i))],[0 1 1 0],'m','facealpha',.1,'edgecolor','r'));
    
    axes(handles.EGRfull);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentEGRfullFill = cat(1,handles.currentEGRfullFill,fill([handles.time(dataStart(i)) handles.time(dataStart(i)) handles.time(dataEnd(i)) handles.time(dataEnd(i))],[0 1 1 0],'m','facealpha',.1,'edgecolor','r'));
    guidata(hObject, handles);
    
    axes(handles.BP_Time);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentBPFill = cat(1,handles.currentBPFill,fill([handles.time(dataStart(i)) handles.time(dataStart(i)) handles.time(dataEnd(i)) handles.time(dataEnd(i))],[90 190 190 90],'m','facealpha',.1,'edgecolor','r'));
    
    axes(handles.BPfull);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentBPfullFill = cat(1,handles.currentBPfullFill,fill([handles.time(dataStart(i)) handles.time(dataStart(i)) handles.time(dataEnd(i)) handles.time(dataEnd(i))],[90 190 190 90],'m','facealpha',.1,'edgecolor','r'));
    guidata(hObject, handles);
    
    axes(handles.parameters);
     for i = 1:length(handles.parameterLim)
                for j = dataStart:dataEnd
                    if(handles.engineSpeed(j)>handles.parameterLim(i,1) && handles.engineSpeed(j)<handles.parameterLim(i,2) && handles.fuelRate(j)>handles.parameterLim(i,3) && handles.fuelRate(j)<handles.parameterLim(i,4))
                        handles.currentParametersFill = [handles.currentParametersFill fill([handles.parameterLim(i,3) handles.parameterLim(i,4) handles.parameterLim(i,4) handles.parameterLim(i,3)],[handles.parameterLim(i,1) handles.parameterLim(i,1) handles.parameterLim(i,2) handles.parameterLim(i,2)],'m','facealpha',.1,'edgecolor','r')];
                        guidata(hObject, handles); 
                        break;
                    end
                end
     end
end
    





% --- Executes on button press in Delete.
function Delete_Callback(hObject, eventdata, handles)
% hObject    handle to Delete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = cellstr(get(handles.selectedList,'String'));
selectIndex = get(handles.selectedList,'Value');
i = 0;
for index = selectIndex(1:end)
    contents(index-i) = [];
    i=i+1;
end
handles.database = [cell2mat(contents)];
guidata(hObject, handles);
set(handles.selectedList,'Value',1);
set(handles.selectedList,'String',{handles.database});


% --- Executes on button press in showAll.
function showAll_Callback(hObject, eventdata, handles)
% hObject    handle to showAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to ShowAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = cellstr(get(handles.selectedList,'String'));

selectIndex = length(contents);

delete(handles.highlights);
        delete(handles.currentBPFill);
        delete(handles.currentBPfullFill);
        delete(handles.currentEGRFill);
        delete(handles.currentEGRfullFill);
        delete(handles.currentParametersFill);
        
for index = 1:selectIndex
selected = contents{index};
selected = str2num(selected);
dataStart = round(selected(1)*handles.dataLength/handles.finalTime);
dataEnd = round(selected(2)*handles.dataLength/handles.finalTime);

        
 
     i = 1;  
    axes(handles.EGR_Rate);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentEGRFill = cat(1,handles.currentEGRFill,fill([handles.time(dataStart(i)) handles.time(dataStart(i)) handles.time(dataEnd(i)) handles.time(dataEnd(i))],[0 1 1 0],'m','facealpha',.1,'edgecolor','r'));
    
    axes(handles.EGRfull);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentEGRfullFill = cat(1,handles.currentEGRfullFill,fill([handles.time(dataStart(i)) handles.time(dataStart(i)) handles.time(dataEnd(i)) handles.time(dataEnd(i))],[0 1 1 0],'m','facealpha',.1,'edgecolor','r'));
    guidata(hObject, handles);
    
    axes(handles.BP_Time);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentBPFill = cat(1,handles.currentBPFill,fill([handles.time(dataStart(i)) handles.time(dataStart(i)) handles.time(dataEnd(i)) handles.time(dataEnd(i))],[90 190 190 90],'m','facealpha',.1,'edgecolor','r'));
    
    axes(handles.BPfull);
    %handles.currentSelect = plot(handles.time(dataStart:dataEnd),handles.egrrate(dataStart:dataEnd),'y');
    handles.currentBPfullFill = cat(1,handles.currentBPfullFill,fill([handles.time(dataStart(i)) handles.time(dataStart(i)) handles.time(dataEnd(i)) handles.time(dataEnd(i))],[90 190 190 90],'m','facealpha',.1,'edgecolor','r'));
    guidata(hObject, handles);
    
    axes(handles.parameters);
     for i = 1:length(handles.parameterLim)
                for j = dataStart:dataEnd
                    if(handles.engineSpeed(j)>handles.parameterLim(i,1) && handles.engineSpeed(j)<handles.parameterLim(i,2) && handles.fuelRate(j)>handles.parameterLim(i,3) && handles.fuelRate(j)<handles.parameterLim(i,4))
                        handles.currentParametersFill = [handles.currentParametersFill fill([handles.parameterLim(i,3) handles.parameterLim(i,4) handles.parameterLim(i,4) handles.parameterLim(i,3)],[handles.parameterLim(i,1) handles.parameterLim(i,1) handles.parameterLim(i,2) handles.parameterLim(i,2)],'m','facealpha',.1,'edgecolor','r')];
                        guidata(hObject, handles); 
                        break;
                    end
                end
     end
end


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.grade = get(hObject,'Value')*100;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in twenty.
function twenty_Callback(hObject, eventdata, handles)
% hObject    handle to twenty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.grade = 20;
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of twenty


% --- Executes on button press in zero.
function zero_Callback(hObject, eventdata, handles)
% hObject    handle to zero (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.grade = 0;
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of zero


% --- Executes on button press in forty.
function forty_Callback(hObject, eventdata, handles)
% hObject    handle to forty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.grade = 40;
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of forty


% --- Executes on button press in sixty.
function sixty_Callback(hObject, eventdata, handles)
% hObject    handle to sixty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.grade = 60;
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of sixty


% --- Executes on button press in eighty.
function eighty_Callback(hObject, eventdata, handles)
% hObject    handle to eighty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.grade = 80;
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of eighty


% --- Executes on button press in hundred.
function hundred_Callback(hObject, eventdata, handles)
% hObject    handle to hundred (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.grade = 100;
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of hundred
